#!

# inspect all the ca certs

function certs {
    ENV=$1
    HOOK=$2
    kubectl config view --minify --raw  -o jsonpath='{$.clusters[].cluster.certificate-authority-data}{"\n"}' > validate/${ENV}_cluster.base64
    cat validate/${ENV}_cluster.base64 | base64 -D > validate/${ENV}_cluster.crt
    openssl x509 -in validate/${ENV}_cluster.crt -text -noout > validate/${ENV}_cluster.txt

    kubectl get ${HOOK}.admissionregistration.k8s.io -o jsonpath='{.items[0].webhooks[0].clientConfig.caBundle}{"\n"}' > validate/${ENV}_webhook.base64
    cat validate/${ENV}_webhook.base64 | base64 -D > validate/${ENV}_webhook.crt
    openssl x509 -in validate/${ENV}_webhook.crt -text -noout > validate/${ENV}_webhook.txt

    kubectl get secrets kubetls-bootstrap  -o jsonpath='{.data.tls\.crt}{"\n"}' > validate/${ENV}_kubetls_bootstrap.base64
    cat validate/${ENV}_kubetls_bootstrap.base64 | base64 -D > validate/${ENV}_kubetls_bootstrap.crt
    openssl x509 -in validate/${ENV}_kubetls_bootstrap.crt -text -noout > validate/${ENV}_kubetls_bootstrap.txt

    openssl verify -verbose -CAfile validate/${ENV}_cluster.crt validate/${ENV}_cluster.crt
    openssl verify -verbose -CAfile validate/${ENV}_cluster.crt validate/${ENV}_webhook.crt
    openssl verify -verbose -CAfile validate/${ENV}_cluster.crt validate/${ENV}_kubetls_bootstrap.crt
    diff validate/${ENV}_cluster.crt validate/${ENV}_webhook.crt
}

#############################################################################

kubectl config use-context kubetls-demo
certs aws validatingwebhookconfigurations


#############################################################################

kubectl config use-context gke_lemonade-demo-main_us-west3-c_cluster-1
certs gkenew validatingwebhookconfigurations

#############################################################################

kubectl config use-context do-sfo2-personal-kube
certs do mutatingwebhookconfigurations

kubectl get secrets kubessl-bootstrap  -o jsonpath='{.data.tls\.crt}{"\n"}' > validate/${ENV}_kubessl_bootstrap.base64
cat validate/${ENV}_kubessl_bootstrap.base64 | base64 -D > validate/${ENV}_kubessl_bootstrap.crt
openssl x509 -in validate/${ENV}_kubessl_bootstrap.crt -text -noout > validate/${ENV}_kubessl_bootstrap.txt


#############################################################################

kubectl config use-context gke_lemonade-demo-main_us-west4-a_lemonade-demo
certs lemonade mutatingwebhookconfigurations


