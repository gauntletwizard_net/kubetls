
function applyBootstrap {
    ENV=$1

    #openssl genrsa -out ${ENV}_certkey.pem 4096
    #openssl ecparam -name prime256v1 -genkey | sed -e "1,3d" > ${ENV}_certkey.pem 
    #openssl req -new -sha256 -key ${ENV}_certkey.pem -subj "/CN=abc/ST=WA" -out ${ENV}_certreq.csr

    echo {} | jq '{"CN":"'${ENV}'-kubtls","names":[{"C":"US","L":"Issaquah","O":"kubssl","OU":"'${ENV}'","ST":"WA"}],"hosts":["kubetls.kubetls.svc","kubetls.default.svc"],}' | cfssl genkey - > ${ENV}_cfsslreq.json
    cat ${ENV}_cfsslreq.json | jq -r '.key?' > ${ENV}_certkey.pem
    cat ${ENV}_cfsslreq.json | jq -r '.csr?' > ${ENV}_certreq.pem
    openssl req -in ${ENV}_certreq.pem -text -noout > ${ENV}_certreq.txt
    echo {} | jq --rawfile csr ${ENV}_certreq.pem '{apiVersion: "certificates.k8s.io/v1beta1", kind: "CertificateSigningRequest", metadata: {name: "kubetls-bootstrap"}, spec: {request: $csr| @base64, usages: ["server auth", "client auth", "digital signature", "key encipherment"]}}' > ${ENV}_certreq.json

    kubectl delete csr kubetls-bootstrap
    kubectl create -f ${ENV}_certreq.json
    kubectl certificate approve kubetls-bootstrap
    kubectl get csr kubetls-bootstrap -o json | jq -r  '.status.certificate | @base64d' > ${ENV}_cert.crt
    openssl x509 -in ${ENV}_cert.crt -text -noout > ${ENV}_cert.txt

    kubectl delete secret kubetls-bootstrap
    kubectl create secret tls kubetls-bootstrap --key ${ENV}_certkey.pem --cert ${ENV}_cert.crt
}

kubectl config use-context kubetls-demo
applyBootstrap aws

kubectl config use-context gke_lemonade-demo-main_us-west3-c_cluster-1
applyBootstrap gkenew

#kubectl config use-context do-sfo2-personal-kube
#applyBootstrap do

kubectl config use-context gke_lemonade-demo-main_us-west4-a_lemonade-demo
applyBootstrap lemonade

