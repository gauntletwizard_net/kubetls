#!

export CI_COMMIT_SHA=manual`date "+%Y%m%d-%H%M%S"`
export CI_REGISTRY_IMAGE=registry.gitlab.com/gauntletwizard_net/kubetls
echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA} .
docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

cat k8s/webhookserver/deployment.yaml | sed -e "s@image: registry.gitlab.com/gauntletwizard_net/kubetls@image: registry.gitlab.com/gauntletwizard_net/kubetls:${CI_COMMIT_SHA}@" | kubectl apply -f -
sleep 1
kubectl get pods


