package main

import (
	"fmt"
	"log"
	"crypto/rsa"
	"crypto/tls"
	"crypto/rand"
	"crypto/x509"
	"io/ioutil"
	"encoding/base64"
	"encoding/pem"
	// GoDoc: https://godoc.org/software.sslmate.com/src/go-pkcs12
	// Issues / Pull Requests: https://github.com/SSLMate/go-pkcs12
	"software.sslmate.com/src/go-pkcs12" 
)

// sample program to convert a PEM encoded Certificate file and
// Private Key file into a PKCS12 formated file which is used
// by the Java Key Store format
func main() {
	log.Printf("p12");

	// load the PEM encoded Certificate and Key files
	var certFile = "/var/tls/tls.crt"
	var keyFile = "/var/tls/tls.key"	
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%d", len(cert.Certificate))
	if (len(cert.Certificate) > 0) {
		log.Printf("%d", len(cert.Certificate[0]))
		sEnc := base64.StdEncoding.EncodeToString(cert.Certificate[0])
		log.Printf("%s\n", sEnc)
	} else {
		log.Fatal("No certificates found in key pair")
	}

	// Parse the bytes in the file as ASN.1 bytes containing a Certificate
	x509Cert, err := x509.ParseCertificate(cert.Certificate[0]);
	if err != nil {
		log.Fatal(err)
	}

	passphrasestring := "DESEncodedPassprhase"
	passphrasebytes := []byte(passphrasestring)

	// Use the sslmate library to create a PKCS12 formated string
	pfxData, err := pkcs12.Encode(rand.Reader, cert.PrivateKey, x509Cert, nil, passphrasestring)
	if err != nil {
		log.Fatal(err)
	}

	pfxEnc := base64.StdEncoding.EncodeToString(pfxData)
	log.Printf("pfxData = %s\n", pfxEnc)

	err = ioutil.WriteFile("/var/tls/tls.p12", pfxData, 0644)
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("/var/tls/p12.password", passphrasebytes, 0644)
	if err != nil {
		log.Fatal(err)
	}
	
	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}


	keyBytes := x509.MarshalPKCS1PrivateKey(key)
	pemBlock := pem.Block{
		Type: "RSA PRIVATE KEY",
		Bytes: keyBytes,
	}
	pemKey := pem.EncodeToMemory(&pemBlock)
	fmt.Println(string(pemKey))
}
