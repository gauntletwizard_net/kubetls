package wellknown

// Well known constants and strings used by kubetls
const (
	KubetTLSSignerName = "gauntletwizard.net/kubetls"

	SPIFFEScheme = "spiffe"
)
