package main

import (
    "log"
    "crypto/x509"
    "crypto/rand"
    "crypto/rsa"
    "crypto/x509/pkix"
    "encoding/pem"
    "os"
)

func main() {
    log.Print("start tls csr")
    // https://stackoverflow.com/a/50394867/7464015

    log.Print(len(os.Args))

    var commonname = "example.com"
    if (len(os.Args) >= 2) {
       commonname = os.Args[1];
    }

    keyBytes, _ := rsa.GenerateKey(rand.Reader, 1024)

    subj := pkix.Name{
        CommonName:         commonname,
	// we can add more here, but it's not necessary
    }

    template := x509.CertificateRequest{
        Subject:            subj,
        SignatureAlgorithm: x509.SHA256WithRSA,
    }

    csrBytes, _ := x509.CreateCertificateRequest(rand.Reader, &template, keyBytes)
    pem.Encode(os.Stdout, &pem.Block{Type: "CERTIFICATE REQUEST", Bytes: csrBytes})

}

