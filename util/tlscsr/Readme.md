# Introduction

This is a command line tool to build a basic X509 Certificate Request.

```
go build
./tlscsr | openssl req -inform pem -text
```
