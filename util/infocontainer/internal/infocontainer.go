package internal

// again, package main is used for executable
// ala spring-boot this the web server is embedded

// https://golang.org/pkg/net/http/

import (
    "fmt"
    "log"
    "net/http"
    "os/exec"
)

type InfoHandler struct {}


func displayCommand(c string, w http.ResponseWriter) {
    fmt.Fprintf(w, "Cmd: <code>%s</code>", c)

    cmd := exec.Command("sh", "-c", c)
    stdoutStderr, err := cmd.CombinedOutput()
    if err != nil {
        fmt.Fprintf(w, "<blockquote>")
        fmt.Fprintf(w, "command return error")
        fmt.Fprintf(w, "</blockquote>")
	return
    }


    fmt.Fprintf(w, "<pre>")
    fmt.Fprintf(w, "%s\n", stdoutStderr)
    fmt.Fprintf(w, "</pre>")
}

func(i InfoHandler) InfoHandler(w http.ResponseWriter, r *http.Request) {
    log.Printf("A URL: %s", r.URL)

    w.Header().Set("Content-Type", "text/html")
    w.WriteHeader(200)

    fmt.Fprintf(w, "<html>")
    fmt.Fprintf(w, "<head>")
    fmt.Fprintf(w, "<style>")
    fmt.Fprintf(w, "code { font-size: 2em; }")
    fmt.Fprintf(w, "pre { font-size: 1.25em; }")
    //fmt.Fprintf(w, "body {background-color: powderblue;}")
    fmt.Fprintf(w, "</style>")
    fmt.Fprintf(w, "</head>")
    fmt.Fprintf(w, "<html>")

    fmt.Fprintf(w, "<body>")
    fmt.Fprintf(w, "Handeler A %s!", r.URL)
    fmt.Fprintf(w, "<br/>")
    fmt.Fprintf(w, "<br/>")

    displayCommand("date", w)
    displayCommand("uname -a", w)
    displayCommand("whoami", w)
    displayCommand("id", w)
    displayCommand("pwd", w)
    displayCommand("env", w)
    displayCommand("ls -l .", w)
    displayCommand("ls -l /var/log", w)
    displayCommand("ls -l /go/src/app", w)
    displayCommand("ls -R /var/run", w)

    fmt.Fprintf(w, "</body>")
    fmt.Fprintf(w, "</html>")
}
