package main

// again, package main is used for executable
// ala spring-boot this the web server is embedded

// https://golang.org/pkg/net/http/

import (
    "log"
    "net/http"

    "gitlab.com/gauntletwizard_net/kubetls/util/infocontainer/internal"
)



func main() {
     log.Printf("start main");
     var spec = ":8080"

     var infohandler = internal.InfoHandler{}     

    // URL pattern strings are described here:
    // https://golang.org/pkg/net/http/#ServeMux

    // tells the http package to call the function "handler" for all inbound requests
    http.HandleFunc("/", infohandler.InfoHandler)

    // https://golang.org/pkg/net/http/#Server.ListenAndServe
    // if ever the webserver returns, it's bad, so it's fatal
    log.Printf("Listen spec: %s", spec);
    log.Fatal(http.ListenAndServe(spec, nil))
}
