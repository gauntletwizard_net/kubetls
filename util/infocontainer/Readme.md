# infocontainer Overview

A server which runs some shell commands to display parts of the running
container and it's OS environnment. Used to make visible the moving
parts of how a web server is running.

# Testing

```
cd cmd
go build
./cmd
```
The server is running as a simple process in your user space. The
information displayed is similar to your user shell process.

You can browse the output via the url http://localhost:8080

# Container

Build and test the container:
```
docker build -t registry.gitlab.com/gauntletwizard_net/kubetls/infocontainer:lastest .

docker run -p 8080:8080 registry.gitlab.com/gauntletwizard_net/kubetls/infocontainer:lastest
```
You can browse the output from the container also using the same url http://localhost:8080
In a contianer the output will be different from above.

Then push the container to the registry:
```
docker push registry.gitlab.com/gauntletwizard_net/kubetls/infocontainer:lastest 
```

# Kubernetes

Deploy to a cluster and resulters will be different again.

```
kubectl apply -f k8s/deployment.yaml
```
