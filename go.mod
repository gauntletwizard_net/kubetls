module gitlab.com/gauntletwizard_net/kubetls

go 1.13

require (
	github.com/prometheus/client_golang v1.9.0
	github.com/sercand/kuberesolver/v3 v3.0.0
	google.golang.org/grpc v1.33.1
	google.golang.org/grpc/examples v0.0.0-20201104172819-c6fa12175f1c
	k8s.io/api v0.19.0
	k8s.io/apimachinery v0.19.0
	k8s.io/client-go v0.19.0
	k8s.io/utils v0.0.0-20200821003339-5e75c0163111 // indirect
	software.sslmate.com/src/go-pkcs12 v0.0.0-20201102150903-66718f75db0e
)
