package com.tcbtech.kubetls;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.net.ssl.SSLContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

// @formatter:off
/**
 * KubeTls Spring Boot Application Setup.
 * 
 * <p>
 * Sets up a Spring Boot application to use TLS and use the server (service)
 * certificate supplied to the pod in the standard location. KubeTls is a
 * project which provides TLS certificates to all pods so that all communication
 * can be easily and natively encrypted with TLS. </p>
 * 
 * <p>Usage: </p>
 * <pre>
 * SpringApplication myApp = new SpringApplication(MyApplicationClass.class); 
 * KubeTls.kubetlsSetup(myApp);
 * myApp.run(args); 
 * </pre>
 */
// @formatter:on
@Component
public class KubeTls {

    private static final Logger log = LoggerFactory.getLogger(KubeTls.class);

    /** Path to certificate store provided by KubeTls. */
    public static final String certStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.certStore";
    /**
     * Path to trusted certificate store provided by KubeTls. Trusted certificates
     * are those provided by the Kubernetes Cluster's Certificate Authority.
     */
    public static final String trustStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.trustStore";
    /**
     * Path to a file which contains the password for the cert and trust store files
     * provided by KubeTls.
     */
    public static final String passwordFile = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.password";

    private KubeTls() {
        // prohibit the use of the constructor
    };

    /**
     * Perform initial TLS configuration setup during the
     * <code>ApplicationEnvironmentPreparedEvent</code> application event.
     * 
     * @param myApp A Spring application.
     * @throws KubeTlsException A runtime exception.
     */
    public static void kubetlsSetup(SpringApplication myApp) throws KubeTlsException {

        log.debug(" app {}", myApp);

        myApp.addListeners(new ApplicationListener<ApplicationEnvironmentPreparedEvent>() {
            @Override
            public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) throws KubeTlsException {

                log.info("KubeTlsSetup - {}", System.getProperty("user.name"));
                log.info("{} - {}", myApp.getClass().toString(), myApp.toString());

                String keyStorePassPhrase;
                try {
                    keyStorePassPhrase = Files.readString(Paths.get(passwordFile));
                } catch (IOException e) {
                    throw new KubeTlsException("error reading pass phrase from " + passwordFile, e.getCause());
                }
                System.setProperty("server.ssl.enabled", "true");
                System.setProperty("server.ssl.key-store", certStore);
                System.setProperty("server.ssl.key-store-password", keyStorePassPhrase);

                log.info("set server.ssl.enabled to true");
                log.info("added certstore: {}", certStore);

                SSLContext.setDefault(KubeTlsContext.getContext());

                if (log.isDebugEnabled()) {
                    DiagnoseTLS.diagnoseKeystore(certStore, keyStorePassPhrase);
                    DiagnoseTLS.diagnoseKeystore(trustStore, keyStorePassPhrase);
                }

            }
        });
    }

}
