
/**
 * KubeTls Library for running Spring Boot Applications with native TLS.
 * 
 * See: https://gitlab.com/gauntletwizard_net/kubetls
 * 
 * KubeTLS automatically adds a generated TLS key and Certificate signed by the
 * cluster CA to every pod created. This library makes it easy for developers
 * use TLS for all communications to and from a Spring Boot Java application.
 * 
 * For information on how KubeTls creates X509 certificates for all pods see the
 * GitLab repository above. To setup your Spring Boot application to use TLS to
 * encrypt all traffic see below.
 * 
 * <p>
 * Usage:
 * </p>
 * 
 * <pre>
 * SpringApplication myApp = new SpringApplication(MyApplicationClass.class);
 * KubeTls.kubetlsSetup(myApp);
 * myApp.run(args);
 * </pre>
 */

package com.tcbtech.kubetls;
