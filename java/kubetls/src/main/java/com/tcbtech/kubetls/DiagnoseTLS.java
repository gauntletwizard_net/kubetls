package com.tcbtech.kubetls;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Print diagnostic information about TLS setup.
 * 
 * Used for debugging various aspects of Spring Framework TLS and Java crypto
 * library information.
 */
public class DiagnoseTLS {

    static Logger log = LoggerFactory.getLogger(DiagnoseTLS.class);

    /**
     * Print out the certificated found in a Java keystore.
     * 
     * @param filename   Keystore to examine.
     * @param passphrase Needed to open the keystore.
     */
    static public void diagnoseKeystore(String filename, String passphrase) {
        log.info("ks: {}", filename);
        try {
            // InputStream certIs = new FileInputStream(filename);
            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(new FileInputStream(filename), passphrase.toCharArray());
            log.info("ks.size: {}", ks.size());
            // Certificate cert = ks.getCertificate("alias");

            for (Enumeration<String> e = ks.aliases(); e.hasMoreElements();) {
                String alias = e.nextElement();
                log.info("alias {}", alias);
                log.info("is isCertificateEntry​ {}", ks.isCertificateEntry(alias));
                log.info("is isKeyEntry​ {}", ks.isKeyEntry(alias));

                if (ks.isCertificateEntry(alias)) {
                    Certificate ksCert = ks.getCertificate(alias);
                    log.info("cert type: {}", ksCert.getType());
                    log.info("cert class: {}", ksCert.getClass().toString());
                    if (ksCert instanceof X509Certificate) {

                    }
                }
            }

            Set<X509Certificate> s = getTrustedCerts(ks);
            log.info("s {}", s);

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // from openkdk-11
    // https://github.com/AdoptOpenJDK/openjdk-jdk11/blob/999dbd4192d0f819cb5224f26e9e7fa75ca6f289/src/java.base/share/classes/sun/security/validator/TrustStoreUtil.java

    /**
     * Extract X509 certificates from a keystore
     * 
     * @param ks Keystore to extract
     * @return Set of the X509 certificates from the provided keystore
     */
    public static Set<X509Certificate> getTrustedCerts(KeyStore ks) {
        Set<X509Certificate> set = new HashSet<>();
        try {
            for (Enumeration<String> e = ks.aliases(); e.hasMoreElements();) {
                String alias = e.nextElement();
                if (ks.isCertificateEntry(alias)) {
                    Certificate cert = ks.getCertificate(alias);
                    if (cert instanceof X509Certificate) {
                        set.add((X509Certificate) cert);
                    }
                } else if (ks.isKeyEntry(alias)) {
                    Certificate[] certs = ks.getCertificateChain(alias);
                    if ((certs != null) && (certs.length > 0) && (certs[0] instanceof X509Certificate)) {
                        set.add((X509Certificate) certs[0]);
                    }
                }
            }
        } catch (KeyStoreException e) {
            // ignore
            //
            // This should be rare, but better to log this in the future.
        }

        return Collections.unmodifiableSet(set);
    }
}
