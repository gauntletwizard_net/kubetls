package com.tcbtech.kubetls;

/**
 * Exceptions froom KubeTls processing.
 */
@SuppressWarnings("serial")
public class KubeTlsException extends RuntimeException {
    /**
     * Create a KubeTls runtime processing exception capturing another target.
     * 
     * @param reason reason for error
     */
    public KubeTlsException(String reason) {
        super(reason);
    }

    /**
     * Create a KubeTls runtime processing exception capturing another target.
     * 
     * @param reason    reason for error
     * @param throwable original error
     */
    public KubeTlsException(String reason, Throwable throwable) {
        super(reason, throwable);
    }
}
