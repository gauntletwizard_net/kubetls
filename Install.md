# Steps to add KubeTLS to a cluster

Ensure that kubetls is built and in a Docker repository

# Bootstrapping

First, run the commands in the `k8s/bootstrap/README.md` file. This creates
an initial certificated used by Kubetls.

# Create a kubetls namespace in your cluster

```
kubectl apply -f k8s/webhookserver/namespace.yaml
kubectl get namespaces
```

# Deploy the kubetls webhook server

Create an image pull secret named `gitlab` in order for the deployment
to read our GitLab docker registry.

```
kubectl create secret docker-registry gitlab --docker-server=registry.gitlab.com --docker-username=markphahn --docker-email=MHahn@TCBTech.com --docker-password=<your-token-here>
```

First create the service account used by kubetls, then the RBAC,
before finally deploying the kubetls pod and service.

```
kubectl apply -f k8s/webhookserver/serviceaccount.yaml
kubectl get serviceaccounts

kubectl apply -f k8s/webhookserver/crb.yaml
kubectl describe clusterrolebindings kubetls

kubectl apply -f k8s/webhookserver/deployment.yaml
kubectl get pods

kubectl apply -f k8s/webhookserver/service.yaml
kubectl get service
```

Ensure the container specification in the `deployment.yaml` file has the
right image tag. (Hint: either modify the `deployment.yaml` file or use
`kubectl edit deployment kubetls`.)

# Insert kubetls as an admission controller web hook

Borrowing the command from the `k8s/bootstrap/README.md` get the
certificate installed in the `kubetls-bootstrap` secret.

```
kubectl config view --minify --raw  -o jsonpath='{$.clusters[].cluster.certificate-authority-data}{"\n"}'
```

Enter this value as the `caBundle` value in
`k8s/webhookserver/webhookcoonfig.yaml`.

Deploy kubetls as a `ValidatingWebhookConfiguration`

```
kubectl apply -f k8s/webhookserver/webhookconfig.yaml
```

# See some results

Just delete a pod. It's deployment will create a new pod to replace
it.  That new pod creation will run through kubetls as the Validating
Webhook.
