package main

import (
	"io/ioutil"
	"log"
	"net/url"
	"os"

	"gitlab.com/gauntletwizard_net/kubetls/tlslib"
)

const (
	address     = "localhost:8443"
	defaultName = "world"
)

func main() {
	request := url.URL{
		Scheme: "https",
		Host:   address,
	}
	if len(os.Args) > 2 {
		request.Host = os.Args[2]
	}
	query := url.Values{
		"name": []string{defaultName},
	}
	if len(os.Args) > 1 {
		query.Set("name", os.Args[1])
	}
	request.RawQuery = query.Encode()
	client, err := tlslib.KubeHTTPClient()
	if err != nil {
		log.Fatalf("Failed to start client, %s", err)
	}
	resp, err := client.Get(request.String())
	if err != nil {
		log.Fatalf("Request failed, %s", err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Invalid read, %s", err)
	}
	log.Print(string(body))
}
