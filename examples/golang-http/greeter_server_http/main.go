// Package main implements a server for Greeter service.
package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/gauntletwizard_net/kubetls/tlslib"
)

const (
	port = ":8443"
)

// server is used to implement helloworld.GreeterServer.
type server struct {
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHello(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	name := r.Form.Get("name")
	log.Printf("Received: %v", name)
	if name == "" {
		name = "world"
	}
	fmt.Fprintf(w, "Hello, %s!", name)
}

func main() {
	s := server{}
	http.HandleFunc("/", s.SayHello)
	err := tlslib.ListenAndServeMTLS(port, nil)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
}
