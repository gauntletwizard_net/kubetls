package com.tcbtech.tlsdemo;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Simple API End Point.
 * 
 * Used for demonstration purposes only.
 */
@RestController
@RequestMapping("/api")
public class ApiEndpointController {
    Logger log = LoggerFactory.getLogger(ApiEndpointController.class);

    @GetMapping("/endpoint")
    public Endpoint getEndpoint(@RequestParam Map<String, String> allRequestParams,
            org.springframework.ui.ModelMap model) {
        // The all request params is going to be used to examine the incomming request,
        // maybe
        log.info(allRequestParams.toString());
        return new Endpoint(allRequestParams);
    }

    @GetMapping("/testinfo")
    public TestInfo getTestInfo() {
        return new TestInfo();
    }

}
