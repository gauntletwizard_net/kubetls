package com.tcbtech.tlsdemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/status")
public class StatusInfo {
    Logger log = LoggerFactory.getLogger(StatusInfo.class);

    @GetMapping(value = "/info", produces = "text/plain")
    public ResponseEntity<?> getEndpoint() {
        StringBuilder sb = new StringBuilder();

        String cmd = "openssl x509 " + " -in /var/run/secrets/gauntletwizard.net/tls/tls.crt" + " -text";

        log.debug("cmd {}", cmd);

        try {
            Process process = Runtime.getRuntime().exec(cmd);

            process.waitFor();

            BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            log.info("openssl std output");
            while ((line = br.readLine()) != null) {
                log.info(line);
                sb.append(line);
                sb.append("\n");
            }

            log.info("openssl error output");
            br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((line = br.readLine()) != null) {
                log.info(line);
                sb.append(line);
                sb.append("\n");
            }

        } catch (InterruptedException e) {
            log.warn(e.toString());
            sb.append(e.toString());
            sb.append("\n");
        } catch (IOException e) {
            log.warn(e.toString());
            sb.append(e.toString());
            sb.append("\n");
        }
        log.info("done");
        return new ResponseEntity<>(sb.toString(), HttpStatus.OK);
    }
}
