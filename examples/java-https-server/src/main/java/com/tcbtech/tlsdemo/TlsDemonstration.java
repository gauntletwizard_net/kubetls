package com.tcbtech.tlsdemo;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.tcbtech.kubetls.KubeTlsException;
import com.tcbtech.kubetls.KubeTls;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;

// sample swagger api
// https://springfox.github.io/springfox/docs/current/

// @SpringBootApplication(scanBasePackages = {"me.ramswaroop.jbot", "example.jbot"})

@EnableAutoConfiguration
@SpringBootApplication(scanBasePackages = { "com.tcbtech.kubetls" })
public class TlsDemonstration {

    static Logger log = LoggerFactory.getLogger(TlsDemonstration.class);

    public static void main(String[] args) throws KubeTlsException {

        SpringApplication myApp = new SpringApplication(TlsDemonstration.class);

        KubeTls.kubetlsSetup(myApp);

        System.setProperty("spring.output.ansi.enabled", "never");
        System.setProperty("server.port", "8080");

        myApp.run(args);
    }

    @Bean
    public Docket endpointApi() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any()).build().pathMapping("/")
                .tags(new Tag("Endpoint Service", "All endpoint apis."));
    }

    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder().deepLinking(true).displayOperationId(false).defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1).defaultModelRendering(ModelRendering.EXAMPLE).displayRequestDuration(false)
                .docExpansion(DocExpansion.NONE).filter(false).maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA).showExtensions(false).showCommonExtensions(false)
                .tagsSorter(TagsSorter.ALPHA).supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                .validatorUrl(null).build();
    }

    static public void LogTlsSecrets() {
        log.info("LogTlsSecrets");
        File tlsSecrets = new File("/var/run/secrets/gauntletwizard.net/tls");
        if (tlsSecrets.exists()) {
            if (tlsSecrets.isDirectory()) {
                for (File f : tlsSecrets.listFiles()) {
                    log.info(f.getAbsolutePath());
                }
            } else {
                log.info("not a directory {}", tlsSecrets.getAbsolutePath());
            }
        } else {
            log.info("missing {}", tlsSecrets.getAbsolutePath());
        }
    }
}
