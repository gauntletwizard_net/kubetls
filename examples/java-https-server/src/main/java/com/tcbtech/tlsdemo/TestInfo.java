package com.tcbtech.tlsdemo;

import java.io.File;

public class TestInfo {
    public TestInfo() {
    }

    public String getTmpExists() {
        File tmp = new File("/tmp");
        if (tmp.exists()) {
            if (tmp.isDirectory()) {
                return "yes";
            } else {
                return "not a directory";
            }
        } else {
            return "missing";
        }
    }

    public String getTmpSecretsExists() {
        File tmpSecrets = new File("/tmp/secrets");
        if (tmpSecrets.exists()) {
            if (tmpSecrets.isDirectory()) {
                return "yes";
            } else {
                return "not a directory";
            }
        } else {
            return "missing";
        }
    }

    public File[] getTmp() {
        File tmp = new File("/tmp");
        if (tmp.exists()) {
            if (tmp.isDirectory()) {
                tmp.listFiles();
            }
        }
        return null;
    }

    public File[] getTmpSecrets() {
        File tmpSecrets = new File("/tmp/secrets");

        if (tmpSecrets.exists()) {
            if (tmpSecrets.isDirectory()) {
                return tmpSecrets.listFiles();
            }
        }
        return null;
    }

    public String getSslKeyStore() {
        return System.getProperty("server.ssl.key-store");
    }

    public String getSslKeyStorePassword() {
        return System.getProperty("server.ssl.key-store-password");
    }
}
