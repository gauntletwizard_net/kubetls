#! 

# Manually deploy the plain tls service. Run with:
# bash ManualDeployTlsServer.sh

export CI_COMMIT_SHA=manual`date "+%Y%m%d-%H%M%S"`
export CI_REGISTRY_IMAGE=registry.gitlab.com/gauntletwizard_net/kubetls/java-tls-server
echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

gradle clean assemble

docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA} .
docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

jsonnet k8s/java-tls-server-service.jsonnet | kubectl apply -f -
kubectl -n java-demo get service java-tls-server -o yaml

jsonnet k8s/java-tls-server-deployment.jsonnet -A repository=${CI_REGISTRY_IMAGE} -A image=${CI_COMMIT_SHA} | kubectl apply -f -
sleep 1
kubectl -n java-demo get pods
