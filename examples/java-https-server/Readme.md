# Overview

This folder contains a sample Swagger API written in Java and Spring
Boot. The application demonstrates how to create a simple api endpoint
with Swagger and then protect the communication to that API with TLS.

The notes below are working notes build the app, create some self
signed TLS keys, configure the app to use them, build a container,
then test run the application.


# Swagger in Spring Boot

Start with a template project from Spring initializr (https://start.spring.io/)
or from the Swagger documentation: https://swagger.io/resources/open-api/.
We used SpringFox https://github.com/springfox/springfox-demos/tree/master/spring-java-swagger

To build our sample, use gradle;
```
./gradlew clean build
```

# Spring Boot with TLS

As a starting point, your Spring Boot applciation can set the
`server.ssl` properties to enable. 

We did this by adding these lines to the `main` program before calling
`SpringApplication.run`:

```
	System.setProperty("server.ssl.enabled", "true");
	System.setProperty("server.ssl.key-store", "/var/tls/tls.p12");
	System.setProperty("server.ssl.key-store-password", keyStorePassPhrase);
```

We use a variable for the key store passphrase because we don't want it in code.


# Generating keys

Before your Spring Boot jar can start, you need to create the certicate file and the
key store password file. There are a nubmer of ways to do this. 

See:
https://www.baeldung.com/spring-boot-https-self-signed-certificate

And:
https://docs.oracle.com/javase/8/docs/technotes/tools/unix/keytool.html

## Using cfssl

We chose to use `cfssl` because is it very convenient and it matches
how our kubetls module works.

Install `cfssl` with homebrew or following instructions at https://github.com/cloudflare/cfssl

```
# create location for tls certificate files
sudo mkdir /var/tls
# change owner of /var/tls to the current user to allow writing files to this directory
sudo chown `whoami` /var/tls

# Use jq to create the ca configuration file in json
echo {} | jq '{ "CN": "kubetls.javaapp.svc", "hosts": [ "localhost" ], "key": {"algo": "ecdsa", "size": 256, }, "names": [ { "O": "kubetls", "C": "US", "ST":"WA","L": "Issaquah", } ], } ' > ca.json
cfssl selfsign localhost ca.json | cfssljson -bare

# mimic file names used in Dockerfile
cp cert-key.pem /var/tls/tls.key
cp cert.pem /var/tls/tls.crt

openssl pkcs12 -export -out /var/tls/tls.p12 -in /var/tls/tls.crt -inkey /var/tls/tls.key -passout pass:abc123
echo -n abc123 > /var/tls/p12.password
ls -ltr /var/tls
```

We create the certificate and key file in PEM format using
`cfssl`. Then we convert them to a PKCS12 format which is the format
used by the standard Java Key Store functions. We store the password
in the `p12.password` file so we can read it from there (so it's not
in the code repository).

# View the results

Run the Spring Boot jar:
```
java -jar build/libs/java-tls-server-0.0.1-SNAPSHOT.jar 
```

Note: the server uses port 8080 (eighty eighty).

With this running you can browse:

https://localhost:8080/api/testinfo - which shows debug information

https://localhost:8080/swagger-ui/index.html - which is the swagger UI

https://localhost:8080/api/endpoint - our "application endpoint"


# Containerization

Next we build a container using a `Dockerfile`.

## Environment Vars for Docker convenience

To make type commands convenient, we use some environment variables
which also match our GitLab pipeline. (See ManualDeployTlsService.sh)

```
export CI_COMMIT_SHA=java-tls-server-`git log -1 --pretty=format:"%h"`
export CI_REGISTRY_IMAGE=registry.gitlab.com/gauntletwizard_net/kubetls/java-tls-server
echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}
```

## Creating and testing the Container

```
docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA} .

docker run -p 8080:8080 --mount type=bind,source=/var/tls,target=/var/tls  ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA} 
```

This will run because it is mounting the `/var/tls` directory into the
container so the Sring Boot application can find the certificate file
and key store password.

Again, you can test this with your browser with the URLs above, e.g.
https://localhost:8080/api/testinfo

Now that it works, push it to a docker registry. We use the registry provided by GitLab:

```
docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}
```

# TODO : clean this up to be only small changes for mtls
remove the java-api stuff, and rely on the java-api namespace

# Setup Kubernetes to run the demo api container

We use `jsonnet` as a templating format for our kubectl files. Install
`jsonnet` with Homebrew or https://jsonnet.org/

Create a namespace for testing and an image pull secret. This needs to
be done only once during setup:

```
export UNAME=<your-user-name>
export EMAIL=<your-registry-email>
kubectl -n java-api create secret docker-registry gitlab --docker-server=${CI_REGISTRY_IMAGE} --docker-username=${UNAME} --docker-email=${EMAIL} --docker-password=<pwd>
kubectl -n java-api describe secret gitlab
```
See also: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#inspecting-the-secret-regcred

Now it is possible to deploy the continaer and the kubetls mechanism
will create your certificate for you and mount it in `/var/tls` like
we simulated above.

```
jsonnet k8s/java-api-deployment.jsonnet -A repository=${CI_REGISTRY_IMAGE} -A image=${CI_COMMIT_SHA} | kubectl apply -f -
```

However, if you are not yet running kubetls in your cluster you can manually create a secret which will be mounted:

```
kubectl -n java-api create secret generic java-api-secret --from-literal=key-store-password=abc123 --from-file=key-store-file=mph.p12 --from-literal=extra-data=for-demonstraion
kubectl -n java-api get secrets
kubectl -n java-api get secret -n java-api java-api
kubectl -n java-api get secret -n java-api java-api -o json
kubectl -n java-api describe secrets/java-api -n java-api

jsonnet k8s/java-api-deployment.jsonnet -A repository=${CI_REGISTRY_IMAGE} -A image=${CI_COMMIT_SHA} | kubectl apply -f -
```

Trying to setup a service for the sample API. This does not work
because the Google load balancer will try to connect to the pod's port
8080 with http, not https. Instead use port-forwarding for debugging.

```
jsonnet k8s/java-api-service.jsonnet | kubectl apply -f -
```

However, there is a way to setup a service on Google that will do
direct TCP connections, but I am not taking the time to search for it now.

```
kubectl -n java-demo port-forward java-tls-server-848b6c868f-9swtw 8080
```
