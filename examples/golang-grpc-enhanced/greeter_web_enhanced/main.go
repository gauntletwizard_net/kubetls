// This is the unabashed mashup of the web and grpc versions into a middleware webserver
package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/gauntletwizard_net/kubetls/tlslib"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sercand/kuberesolver/v3"
	"google.golang.org/grpc"
	pb "google.golang.org/grpc/examples/helloworld/helloworld"
)

const (
	address     = "kubernetes:///greeter-server-enhanced:50051"
	defaultName = "world"
)

// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.GreeterClient
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHelloWeb(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	name := r.Form.Get("name")
	log.Printf("Received: %v", name)
	if name == "" {
		name = "world"
	}
	req := &pb.HelloRequest{Name: name}
	resp, err := s.GreeterClient.SayHello(r.Context(), req)
	if err != nil {
		w.WriteHeader(501)
		fmt.Fprintf(w, "Failure from backend: %s", err)
	}
	fmt.Fprintf(w, "Hello, %s!", resp.GetMessage())
}

func main() {
	kuberesolver.RegisterInCluster()
	addr := address
	if len(os.Args) > 2 {
		addr = os.Args[2]
	}
	// Set up a connection to the server.
	conn, err := grpc.Dial(addr, tlslib.WithKubeTLSClientCreds(), grpc.WithBlock(), grpc.WithTimeout(2*time.Second))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	greeterclient := pb.NewGreeterClient(conn)

	s := server{greeterclient}

	http.HandleFunc("/", s.SayHelloWeb)
	http.Handle("/metrics", promhttp.Handler())
	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

}
