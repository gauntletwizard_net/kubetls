#!

export CI_COMMIT_SHA=manual`date "+%Y%m%d-%H%M%S"`
export CI_REGISTRY_IMAGE=registry.gitlab.com/gauntletwizard_net/kubetls/java-client
echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

gradle clean assemble

docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA} .
docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

jsonnet k8s/java-client-job.jsonnet -A repository=${CI_REGISTRY_IMAGE} -A image=${CI_COMMIT_SHA} | kubectl apply -f -
