package com.tcbtech.apidemo;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.time.Duration;

import javax.net.ssl.SSLContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;

// sample swagger api
// https://springfox.github.io/springfox/docs/current/

@SpringBootApplication
public class ClientDemonstration {

    static Logger log = LoggerFactory.getLogger(ClientDemonstration.class);

    public static void main(String[] args) throws IOException, GeneralSecurityException, InterruptedException {

        log.info("start - {}", System.getProperty("user.name"));
        log.info("java.version - {}", System.getProperty("java.version"));

        // removed ansi colorized output because is messes with scripted builds
        System.setProperty("spring.output.ansi.enabled", "never");

        LogTlsSecrets();

        final String certStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.certStore";
        final String trustStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.trustStore";
        final String keyStorePassPhrase = Files
                .readString(Paths.get("/var/run/secrets/gauntletwizard.net/tls/pkcs12.password"));

//        DiagnoseTLS.diagnoseKeystore(certStore, keyStorePassPhrase);
//        DiagnoseTLS.diagnoseKeystore(trustStore, keyStorePassPhrase);

        System.setProperty("javax.net.debug", "all");
        System.setProperty("java.security.debug", "all");

        System.setProperty("server.ssl.enabled", "true");
        System.setProperty("server.ssl.key-store", certStore);
        System.setProperty("server.ssl.key-store-password", keyStorePassPhrase);

        SSLContext.setDefault(KubeTls.getContext());

        final HttpClient httpClient = HttpClient.newBuilder().sslContext(KubeTls.getContext())
                .connectTimeout(Duration.ofSeconds(100)).build();

        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://java-mtls-server:8090/api/endpoint"))
                .build();

        HttpResponse<String> response = httpClient.send(request, BodyHandlers.ofString());

    }

    @Bean
    public Docket endpointApi() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any()).build().pathMapping("/")
                .tags(new Tag("Endpoint Service", "All endpoint apis."));
    }

    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder().deepLinking(true).displayOperationId(false).defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1).defaultModelRendering(ModelRendering.EXAMPLE).displayRequestDuration(false)
                .docExpansion(DocExpansion.NONE).filter(false).maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA).showExtensions(false).showCommonExtensions(false)
                .tagsSorter(TagsSorter.ALPHA).supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                .validatorUrl(null).build();
    }

    static public void LogTlsSecrets() {
        log.info("LogTlsSecrets");
        File tlsSecrets = new File("/var/run/secrets/gauntletwizard.net/tls");
        if (tlsSecrets.exists()) {
            if (tlsSecrets.isDirectory()) {
                for (File f : tlsSecrets.listFiles()) {
                    log.info(f.getAbsolutePath());
                }
            } else {
                log.info("not a directory {}", tlsSecrets.getAbsolutePath());
            }
        } else {
            log.info("missing {}", tlsSecrets.getAbsolutePath());
        }
    }
}
