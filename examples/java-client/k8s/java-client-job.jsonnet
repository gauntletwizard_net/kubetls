//
// jsonnet java-api-depoloyment.jsonnet -A repository=${REG} -A image=${TAG}
//
function(repository, image) {
    apiVersion: "batch/v1",
    kind: "Job",
    metadata: {
	name: "java-client",
	namespace: "java-demo",
    },
    spec: {
    	template: {
	    spec: {
		restartPolicy: "Never",
		containers: [
		    {
			name: "java-client-job",
			imagePullPolicy: "Always",
			image: '%s:%s' % [repository, image],
			env: [
			    {
				name: "container-tag",
				value: image,
			    },
			    {
				name: "namespace",
				valueFrom:  {
				    fieldRef: {
					fieldPath: "metadata.namespace",
				    },
				},
			    },
			],
			resources: {
			    requests: {
				cpu: "50m",
				memory: "250Mi",
			    },
			    limits: {
				cpu: "250m",
				memory: "1000Mi",
			    },
			},
			securityContext: {
			    readOnlyRootFilesystem: true,
			},
			volumeMounts: [
			    {
				name: "tmp",
				mountPath: "/tmp",
			    },
			],
		    },
		],
		volumes: [
		    {
			name: "tmp",
			emptyDir: {},
		    },
		],
		imagePullSecrets: [
		    {
			name: "gitlab", 
		    }
		],
	    }
	}
    }
}

