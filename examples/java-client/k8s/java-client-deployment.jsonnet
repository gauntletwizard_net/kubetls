//
// jsonnet java-api-depoloyment.jsonnet -A repository=${REG} -A image=${TAG}
//
function(repository, image) {
    apiVersion: "apps/v1",
    kind: "Deployment",
    metadata: {
	name: "java-client",
	namespace: "java-demo",
    },
    spec: {
	selector: { 
	    matchLabels: {
		app: "java-client",
		deploy: "java-client-deploy",
	    },
	},
	replicas: 1, 
	template: {
	    // create pods using pod definition in this template
	    metadata: {
		labels: {
		    app: "java-client",
		    deploy: "java-client-deploy",
		    shard: "main",
		},
	    },
	    spec: {
		containers: [
		    {
			name: "java-client-deploy",
			imagePullPolicy: "Always",
			image: '%s:%s' % [repository, image],
			ports: [
			    {
				containerPort: 8080,
			    },
			],
			env: [
			    {
				name: "container-tag",
				value: image,
			    },
			    {
				name: "namespace",
				valueFrom:  {
				    fieldRef: {
					fieldPath: "metadata.namespace",
				    },
				},
			    },
			],
			resources: {
			    requests: {
				cpu: "50m",
				memory: "250Mi",
			    },
			    limits: {
				cpu: "250m",
				memory: "1000Mi",
			    },
			},
			securityContext: {
			    readOnlyRootFilesystem: true,
			},
			volumeMounts: [
			    {
				name: "tmp",
				mountPath: "/tmp",
			    },
			],
		    },
		],
		volumes: [
		    {
			name: "tmp",
			emptyDir: {},
		    },
		],
		imagePullSecrets: [
		    {
			name: "gitlab", 
		    }
		],
	    }
	}
    }
}

