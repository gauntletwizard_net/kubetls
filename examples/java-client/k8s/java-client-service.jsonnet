//
// jsonnet java-client-service.jsonnet
//

// Trying to setup a service for the sample API. This does not work
// because the Google load balancer will try to connect to the pod's port
// 8080 with http, not https. Instead use port-forwarding for debugging.

function() {
    apiVersion: "v1",
    kind: "Service",
    metadata: {
	name: "java-client",
	namespace: "java-demo",
    },
    spec: {
	type: "ClusterIP",
	ports: [
	    {
		name: "java-client-service",
		protocol: "TCP",
		port: 80,
		targetPort: 8080,
	    },
	],
	selector: {
	    app: "java-client",
	},
    },
}
	    

	
