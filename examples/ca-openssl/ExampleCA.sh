#!
# See: https://jamielinux.com/docs/openssl-certificate-authority/create-the-root-pair.html

# mkdir certs crl newcerts private
# chmod 700 private
rm index.txt
touch index.txt
echo 1000 > serial

openssl ecparam -name prime256v1 -genkey > private/ca.key.pem
openssl req -config openssl.cnf -key private/ca.key.pem -new -x509 -extensions v3_ca -out certs/ca.cert.pem -subj "/CN=CA"
openssl x509 -in certs/ca.cert.pem -text

# dev 1
cat > req.conf <<EOF
prompt = no
req_extensions = req_ext
distinguished_name = dn
[ dn ]
CN = dev
[ req_ext ]
subjectAltName = @alt_names
[ alt_names ]
DNS.1 = your-new-domain.com
DNS.2 = www.your-new-domain.com
EOF

openssl ecparam -name prime256v1 -genkey > dev-key.pem
openssl req -new -key dev-key.pem -out dev.csr -config req.conf
openssl req -in dev.csr -text

openssl ca -config openssl.cnf -extensions server_cert -in dev.csr -batch -out dev-cert.pem
openssl x509 -in dev-cert.pem -text

# dev 2
cat > req2.conf <<EOF
prompt = no
req_extensions = req_ext
distinguished_name = dn
[ dn ]
CN = dev2
[ req_ext ]
subjectAltName = DNS:dev2.svc
EOF

openssl ecparam -name prime256v1 -genkey > dev2-key.pem
openssl req -new -key dev2-key.pem -out dev2.csr -config req2.conf
cat req2.conf
openssl req -in dev2.csr -text

openssl ca -config openssl.cnf -extensions server_cert -in dev2.csr -batch -out dev2-cert.pem
openssl x509 -in dev2-cert.pem -text
