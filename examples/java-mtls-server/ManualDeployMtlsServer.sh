#!

# Manually deploy the mutual tls (mtls) service. Run with:
# bash ManualDeployMtlsServer.sh

export CI_COMMIT_SHA=manual`date "+%Y%m%d-%H%M%S"`
export CI_REGISTRY_IMAGE=registry.gitlab.com/gauntletwizard_net/kubetls/java-mtls-server
echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

gradle clean assemble

docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA} .
docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}

jsonnet k8s/java-mtls-server-service.jsonnet | kubectl apply -f -
kubectl -n java-demo get service java-mtls-server -o yaml

jsonnet k8s/java-mtls-server-deployment.jsonnet -A repository=${CI_REGISTRY_IMAGE} -A image=${CI_COMMIT_SHA} | kubectl apply -f -
sleep 1
kubectl -n java-demo get pods
