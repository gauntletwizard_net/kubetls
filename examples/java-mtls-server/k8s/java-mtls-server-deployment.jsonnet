//
// jsonnet java-mtls-server-depoloyment.jsonnet -A repository=${REG} -A image=${TAG}
//
function(repository, image) {
    apiVersion: "apps/v1",
    kind: "Deployment",
    metadata: {
	name: "java-mtls-server",
	namespace: "java-demo",
    },
    spec: {
	selector: { 
	    matchLabels: {
		app: "java-mtls-server",
		deploy: "java-mtls-server-deploy",
	    },
	},
	replicas: 1, 
	template: {
	    // create pods using pod definition in this template
	    metadata: {
		labels: {
		    app: "java-mtls-server",
		    deploy: "java-mtls-server-deploy",
		    shard: "main",
		},
	    },
	    spec: {
		containers: [
		    {	name: "java-mtls-server-deploy",
			imagePullPolicy: "Always",
			image: '%s:%s' % [repository, image],
			ports: [
			    {
				containerPort: 8090,
			    },
			],
			env: [
			    {
				name: "container-tag",
				value: image,
			    },
			    {
				name: "namespace",
				valueFrom:  {
				    fieldRef: {
					fieldPath: "metadata.namespace",
				    },
				},
			    },
			],
			resources: {
			    requests: {
				cpu: "50m",
				memory: "100Mi",
			    },
			    limits: {
				cpu: "250m",
				memory: "500Mi",
			    },
			},
			securityContext: {
			    readOnlyRootFilesystem: true,
			},
			volumeMounts: [
			    {
				name: "tmp",
				mountPath: "/tmp",
			    },
			],
		    },
		],
		volumes: [
		    {
			name: "tmp",
			emptyDir: {},
		    },
		],
		imagePullSecrets: [
		    {
			name: "gitlab", 
		    }
		],
	    }
	}
    }
}

