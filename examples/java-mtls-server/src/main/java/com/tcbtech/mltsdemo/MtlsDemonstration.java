package com.tcbtech.mltsdemo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;

import javax.net.ssl.SSLContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;

// sample swagger api
// https://springfox.github.io/springfox/docs/current/

//-Djavax.net.ssl.trustStore=%CLIENT_CERT%  -Djavax.net.ssl.trustStorePassword=endeca
// -Djavax.net.ssl.trustStore=/var/run/secrets/gauntletwizard.net/tls/pkcs12.trustStore -Djavax.net.ssl.trustStorePassword=changeit
//
// -Djava.security.egd=file:/dev/./urandom 
// -Djava.security.debug=all 
// -Djavax.net.ssl.trustStore=/var/run/secrets/gauntletwizard.net/tls/pkcs12.trustStore -Djavax.net.ssl.trustStorePassword=changeit

@SpringBootApplication
public class MtlsDemonstration {

    static Logger log = LoggerFactory.getLogger(MtlsDemonstration.class);

    public static void main(String[] args) throws IOException, GeneralSecurityException {

        log.info("start - {}", System.getProperty("user.name"));

        // removed ansi colorized output because is messes with scripted builds
        System.setProperty("spring.output.ansi.enabled", "never");

        LogTlsSecrets();

        final String certStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.certStore";
        final String trustStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.trustStore";
        final String keyStorePassPhrase = Files
                .readString(Paths.get("/var/run/secrets/gauntletwizard.net/tls/pkcs12.password"));

//        DiagnoseTLS.diagnoseKeystore(certStore, keyStorePassPhrase);
//        DiagnoseTLS.diagnoseKeystore(trustStore, keyStorePassPhrase);

        System.setProperty("javax.net.debug", "all");
        System.setProperty("java.security.debug", "all");

//        System.setProperty("javax.net.ssl.trustStore", trustStore);
//        System.setProperty("javax.net.ssl.trustStore-password", keyStorePassPhrase);

        System.setProperty("server.ssl.enabled", "true");
        System.setProperty("server.ssl.key-store", certStore);
        System.setProperty("server.ssl.key-store-password", keyStorePassPhrase);

        SSLContext.setDefault(KubeTls.getContext());
        System.setProperty("server.ssl.client-auth", "need");

        System.setProperty("server.port", "8090");
        SpringApplication.run(MtlsDemonstration.class, args);
    }

    @Bean
    public Docket endpointApi() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any()).build().pathMapping("/")
                .tags(new Tag("Endpoint Service", "All endpoint apis."));
    }

    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder().deepLinking(true).displayOperationId(false).defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1).defaultModelRendering(ModelRendering.EXAMPLE).displayRequestDuration(false)
                .docExpansion(DocExpansion.NONE).filter(false).maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA).showExtensions(false).showCommonExtensions(false)
                .tagsSorter(TagsSorter.ALPHA).supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                .validatorUrl(null).build();
    }

    static public void LogTlsSecrets() {
        log.info("LogTlsSecrets");
        File tlsSecrets = new File("/var/run/secrets/gauntletwizard.net/tls");
        if (tlsSecrets.exists()) {
            if (tlsSecrets.isDirectory()) {
                for (File f : tlsSecrets.listFiles()) {
                    log.info(f.getAbsolutePath());
                }
            } else {
                log.info("not a directory {}", tlsSecrets.getAbsolutePath());
            }
        } else {
            log.info("missing {}", tlsSecrets.getAbsolutePath());
        }
    }
}
