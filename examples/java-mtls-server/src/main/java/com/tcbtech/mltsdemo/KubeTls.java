package com.tcbtech.mltsdemo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Assist in setting up mlts calls
 * 
 * Call a service and recognize the kubetls/kubernetes certificate authority for
 * the server provided certificate. This is necesary because the servers TLS
 * certifice is create by a private certificate authority, namely
 * kubetls/kubernetes.
 * 
 * Reference: https://milosgarunovic.com/posts/java-mtls-http-client/
 * 
 * called with ```
 * 
 * 
 * @author markhahn
 *
 */
public class KubeTls {

    private static KubeTls kubeTls = null;

    private final SSLContext sslContext;

    private KubeTls() throws IOException, GeneralSecurityException {

        final String certStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.certStore";
        final String trustStore = "/var/run/secrets/gauntletwizard.net/tls/pkcs12.trustStore";
        final String keyStorePassPhrase = Files
                .readString(Paths.get("/var/run/secrets/gauntletwizard.net/tls/pkcs12.password"));
        sslContext = createAndGetSSLContext(certStore, trustStore, keyStorePassPhrase, keyStorePassPhrase);

    }

    private SSLContext createAndGetSSLContext(String keyStore, String trustStore, String keyStorePassword,
            String trustStorePassword) throws IOException, GeneralSecurityException {

        final KeyManager[] keyManagers = getKeyManagers(keyStore, keyStorePassword);
        final TrustManager[] trustManagers = getTrustManagers(trustStore, trustStorePassword);
        final SSLContext sslContext = SSLContext.getInstance("SSL");

        sslContext.init(keyManagers, trustManagers, null);

        return sslContext;
    }

    private KeyManager[] getKeyManagers(String keyStore, String keyStorePassword)
            throws IOException, GeneralSecurityException {

        String alg = KeyManagerFactory.getDefaultAlgorithm();
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(alg);

        FileInputStream fis = new FileInputStream(keyStore);
        KeyStore ks = KeyStore.getInstance("jks");
        ks.load(fis, keyStorePassword.toCharArray());
        fis.close();

        keyManagerFactory.init(ks, keyStorePassword.toCharArray());

        return keyManagerFactory.getKeyManagers();
    }

    private TrustManager[] getTrustManagers(String trustStore, String trustStorePassword)
            throws IOException, GeneralSecurityException {

        String alg = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(alg);

        KeyStore ks = KeyStore.getInstance(new File(trustStore), trustStorePassword.toCharArray());

        trustManagerFactory.init(ks);

        return trustManagerFactory.getTrustManagers();
    }

    public static SSLContext getContext() throws IOException, GeneralSecurityException {
        if (kubeTls == null) {
            kubeTls = new KubeTls();
        }
        return kubeTls.sslContext;
    }

    public static void CreateCustomTrustManager(String trustStore, String trustStorePassphrase)
            throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException,
            KeyManagementException {

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        // Using null here initializes the TMF with the default trust store.
        tmf.init((KeyStore) null);

        // Get hold of the default trust manager
        X509TrustManager defaultTm = null;
        for (TrustManager tm : tmf.getTrustManagers()) {
            if (tm instanceof X509TrustManager) {
                defaultTm = (X509TrustManager) tm;
                break;
            }
        }

        FileInputStream myKeys = new FileInputStream(trustStore);

        // Do the same with your trust store this time
        // Adapt how you load the keystore to your needs
        KeyStore myTrustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        myTrustStore.load(myKeys, trustStorePassphrase.toCharArray());

        myKeys.close();

        tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(myTrustStore);

        // Get hold of the default trust manager
        X509TrustManager myTm = null;
        for (TrustManager tm : tmf.getTrustManagers()) {
            if (tm instanceof X509TrustManager) {
                myTm = (X509TrustManager) tm;
                break;
            }
        }

        // Wrap it in your own class.
        final X509TrustManager finalDefaultTm = defaultTm;
        final X509TrustManager finalMyTm = myTm;
        X509TrustManager customTm = new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                // If you're planning to use client-cert auth,
                // merge results from "defaultTm" and "myTm".
                return finalDefaultTm.getAcceptedIssuers();
            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                try {
                    finalMyTm.checkServerTrusted(chain, authType);
                } catch (CertificateException e) {
                    // This will throw another CertificateException if this fails too.
                    finalDefaultTm.checkServerTrusted(chain, authType);
                }
            }

            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                // If you're planning to use client-cert auth,
                // do the same as checking the server.
                finalDefaultTm.checkClientTrusted(chain, authType);
            }

        };

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[] { customTm }, null);

        // You don't have to set this as the default context,
        // it depends on the library you're using.
        SSLContext.setDefault(sslContext);
    }

}
