package com.tcbtech.mltsdemo;

import java.time.LocalDateTime;
import java.util.Map;

public class Endpoint {
    final String myClass = this.getClass().toString();
    final String apiTime;
    // final ServletServerHttpRequest request;
    // final WebRequest request;
    Map<String, String> requestParams;

    // public Endpoint(ServletServerHttpRequest request) {
    // public Endpoint(WebRequest request) {
    public Endpoint(Map<String, String> allRequestParams) {
        apiTime = LocalDateTime.now().toString();
        requestParams = allRequestParams;
    }

    public String getMyClass() {
        return myClass;
    }

    public String getApiTime() {
        return apiTime;
    }

    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    // public ServletServerHttpRequest getServletServerHttpRequest() {
    // return request;
    // }

    public Map<String, String> getWebRequest() {
        return requestParams;
    }
}
