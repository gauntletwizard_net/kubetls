# docker build -t registry.gitlab.com/gauntletwizard_net/kubetls .
# docker push registry.gitlab.com/gauntletwizard_net/kubetls

FROM golang:buster as build

WORKDIR /go/src/gitlab.com/gauntletwizard_net/kubetls/webhookserver

COPY go.mod go.mod
COPY go.sum go.sum
RUN go mod download

COPY . .
RUN go install -v ./...

FROM debian:buster

COPY --from=build /go/bin /go/bin

CMD ["/go/bin/webhookserver"]
