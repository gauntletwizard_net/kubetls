package tlslib

/* Package tlslib provides helper functions for use with net/nttp and google.golang.org/grpc
 */

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	KubeTLSSecretLocation = "/var/run/secrets/gauntletwizard.net/tls/"
)

var (
	KubeTLSKeyLocation  = flag.String("mtlsKey", KubeTLSSecretLocation+"tls.key", "File containing MTLS Key")
	KubeTLSCertLocation = flag.String("mtlsCert", KubeTLSSecretLocation+"tls.crt", "File containing MTLS Certificate")
	KubeTLSCALocation   = flag.String("mtlsCA", KubeTLSSecretLocation+"ca.crt", "File containing Trust Bundle")
)

// ListenAndServeMTLS is a wrapper like http.ListenAndServe but with sane defaults for use with KubeTLS.
// Specifically, it sets the TlS configuration to
func ListenAndServeMTLS(addr string, Handler http.Handler) error {
	rootCertData, err := ioutil.ReadFile(*KubeTLSCALocation)
	if err != nil {
		return err
	}
	pool := x509.NewCertPool()
	if !pool.AppendCertsFromPEM(rootCertData) {
		return fmt.Errorf("Failed to parse Kubernetes CA file %s", *KubeTLSCALocation)
	}
	server := &http.Server{Addr: addr, Handler: Handler}
	server.TLSConfig = &tls.Config{
		ClientAuth: tls.RequireAndVerifyClientCert,
		ClientCAs:  pool,
	}
	return server.ListenAndServeTLS(*KubeTLSCertLocation, *KubeTLSKeyLocation)
}

// NewKubeTLS constructs a TLS Configuration for use with KubeTLS, suitable for use in both client and server
// contexts
func NewKubeTLS() (*tls.Config, error) {
	certificate, err := tls.LoadX509KeyPair(*KubeTLSCertLocation, *KubeTLSKeyLocation)
	if err != nil {
		return nil, fmt.Errorf("Failed to initialize K8s Keypair; %s", err)
	}
	rootCertData, err := ioutil.ReadFile(*KubeTLSCALocation)
	if err != nil {
		return nil, fmt.Errorf("Failed to initialize K8s CA Pool; %s", err)
	}
	clientpool := x509.NewCertPool()
	if !clientpool.AppendCertsFromPEM(rootCertData) {
		return nil, fmt.Errorf("Failed to initialize K8s CA Pool; Empty pool")
	}
	// ServerPool includes the kubernetes CA, but also all public CAs (in case of proxy)
	serverpool, err := x509.SystemCertPool()
	serverpool.AppendCertsFromPEM(rootCertData)
	return &tls.Config{
		// Server settings
		Certificates: []tls.Certificate{certificate},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    clientpool,
		// Client Settings
		RootCAs: serverpool,
	}, nil
}

func KubeHTTPClient() (c http.Client, err error) {
	tls, err := NewKubeTLS()
	if err != nil {
		return
	}

	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.TLSClientConfig = tls
	c.Transport = transport
	return
}

// WithKubeTLSServerCreds is like grpc.WithTransportCredentials using KubeTLS's supplied
// It panics if the Kubernetes Credentials are not available
func WithKubeTLSServerCreds() grpc.ServerOption {
	return grpc.Creds(NewKubeTLSCredentialsOrDie())
}

// NewKubeServerTLS is credentials.NewServerTLSFromFile with KubeTLS defaults
func NewKubeTLSCredentials() (credentials.TransportCredentials, error) {
	creds, err := NewKubeTLS()
	if err != nil {
		return nil, err
	}
	return credentials.NewTLS(creds), nil
}

// NewKubeServerTLS is credentials.NewServerTLSFromFile with KubeTLS defaults
func NewKubeTLSCredentialsOrDie() credentials.TransportCredentials {
	creds, err := NewKubeTLS()
	if err != nil {
		panic(err)
	}
	return credentials.NewTLS(creds)
}

// WithKubeTLSClientCreds is like grpc.WithTransportCredentials using KubeTLS's supplied
// It panics if the Kubernetes Credentials are not available
func WithKubeTLSClientCreds() grpc.DialOption {
	return grpc.WithTransportCredentials(NewKubeTLSCredentialsOrDie())
}

// NewKubeClientTLSWithoutM is credentials.NewClientTLSFromFile with KubeTLS defaults
func NewKubeClientTLSWithoutM() (credentials.TransportCredentials, error) {
	return credentials.NewClientTLSFromFile(*KubeTLSCALocation, "")
}

// WithKubeTLSClientCreds is like grpc.WithTransportCredentials using KubeTLS's supplied
// It panics if the Kubernetes Credentials are not available
func WithKubeTLSClientCredsWithoutM() grpc.DialOption {
	creds, _ := NewKubeClientTLSWithoutM()
	return grpc.WithTransportCredentials(creds)
}
