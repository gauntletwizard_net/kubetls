package tlslib

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"

	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/peer"
)

/* Functions in this package manipulate KubeTLS Client Certificates
 * They are building blocks for an authentication system, though
 * care should be taken.
 */

func UserFromGrpcContext(ctx context.Context) (string, error) {
	peerInfo, ok := peer.FromContext(ctx)
	if !ok {
		return "", fmt.Errorf("Not an authenticated GRPC Context")
	}

	switch p := peerInfo.AuthInfo.(type) {
	case credentials.TLSInfo:
		return UserFromConnectionState(&p.State)
	}

	return "", fmt.Errorf("Authenticaiton info error")
}

func UserFromHTTPRequest(r http.Request) (string, error) {
	return UserFromConnectionState(r.TLS)
}

func UserFromConnectionState(conn *tls.ConnectionState) (string, error) {
	if len(conn.PeerCertificates) < 1 {
		return "", fmt.Errorf("No Peer certificate present")
	}
	return conn.PeerCertificates[0].Subject.CommonName, nil
}
