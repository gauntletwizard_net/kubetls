# Overview

This project adds SSL certificates to every pod in a Kubernetes cluster
via pod creation webhook added for the entire cluster.

# Interface:
The KubeTLS Admission Webhook automatically adds a generated TLS key and Certificate signed by the cluster CA to `/var/run/secrets/gauntletwizard.net/tls/tls.{crt,key}`. These are PEM formatted keys in forms typically used by webservers and http clients. Servers should use the cluster's CA certificate (available via `/var/run/secrets/kubernetes.io/serviceaccount/ca.crt`) to verify client certificates. Examples are provided for setting policies to limit connectiions by namespace, by serviceaccount, or via hook.

# Whats in this repository

This repository contains `kubetls`, written in golang, which is the
code which provide the TLS certificates to every pod. And it also
contains language libraries, examples and utility code.

## Kubetls

`kubetls` is
defined by:

* `cmd/` - the startup for the `kubetls` webhook server
* `webhookserver/` - the main code for `kubetls`
* `go.mod` and `go.sum` - version specificaions for webhookserver
* `Dockerfile` - the container specification for `kubetls` webhookserver
* `k8s/` - The kubernetes deployment specifications for bootstrapping and
  deployment
* `Install.md` - Instructions for running `kubetls` on your clusters

## Support

The following folders contian support code for the `kubetls` project.

* `presentation/` - Project presentation for SRECon Americas 20
  (https://www.usenix.org/conference/srecon20americas)
* `tlslib/` - golang suppoort library to support using `kubetls` certificates
* `java/` - java suppoort library to support using `kubetls` certificates
* `util/` - Utility and experimental code
  * `infocontainer/` - Container project which displays runtime environment information used to display the results of KubeTLS
  * `p12/` - Small Golang program to experiment with certificate formats.
  * `tlscsr/` - Small Golang program to build a basic X509 certificate request.
* `examples/` - folder with examples for different languages
  * `ca-openssl/` - example of how to run a certficiate authority (CA) with
    OpenSSL. It's a CA the hard way; use `kubetls` instead.
  * `ca-cfnssl/` - example of how to run a certficiate authority (CA)
    with Cloud Flair's `cfssl` tool. It's easier than using OpenSSL
    but it is still hard; use `kubetls` instead.
  * `java-https-server/` - Java Spring Boot service which uses KubeTls for TLS support.
  * `java-mtls-serve/`r - Java Spring Boot service which uses KubeTls to support Mutual TLS with a client.
  * `java-client/` - Java Spring Boot client which uses KubeTls to authenticate to Mutual TLS service.
