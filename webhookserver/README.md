# webhook Overview

A Kubernetes webhook server for Kubernetes AdmissionRequest:  
https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#request  

This creates a container to run an admission controller which will
create a TLS certificate for each pod admitted to the cluster.

## Kubernetes Objects created by WebhookServer
On pod creation, the webhook will attempt to generate or reuse a secret signed with the appropriate set of services. It will also create a CSR for each secret such that Kubernetes creates the 

### CSR:
* Labels: {kubetls: owned}
* Annotations:
  * kubetls.gauntletwizard.net/services: service-list (i.e. default/foo, default/bar)

### Secret:
* Labels: {kubetls: owned}
* Annotations:
  * kubetls.gauntletwizard.net/services:

## Format of a certificate request
When generating a certificate, a useful but standards-compliant certificate is the goal. The certificate should have DNSNames (Also known as SubjectAlternativeNames) for each common DNS address that the pod might be known as, including services (of the form "servicename", as well as "servicename.cluster-domain.example"). The CommonName should be the name of the service account, to match the user format: https://kubernetes.io/docs/reference/access-authn-authz/authentication/#x509-client-certs ; i.e. `system:serviceaccount:namespace:serviceaccountname`

Usages include both "server auth" and "client auth"; The certificate is meant to be used for both ends of a MTLS connection.

# cmd
The webhook server is started from the main fuction in `/cmd/webhookserver/main.go`:
https://github.com/golang-standards/project-layout/blob/master/cmd/README.md
