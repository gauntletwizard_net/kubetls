package tlsService

import (
	"context"
	"crypto/x509"
	"testing"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/fake"
)

func simplePod(name string, labels map[string]string) corev1.Pod {

	return corev1.Pod{}
}

func simpleInfo(name string, services []string) ServicesTLSKey {
	return ServicesTLSKey{}
}

func simpleSecret(name string, data map[string]string) {

}

// Needs the modifications in mhahn's branch before we write tests
func TestCSRGeneration(t *testing.T) {
}

// Disabled, until I extend fakeClientSet to approve
func testSecretCreation(t *testing.T) {
	secrets := []runtime.Object{}
	tls := NewRealTlsService(fake.NewSimpleClientset(secrets...), DefaultKeyLength, "test.gauntletwizard.net/kubetls", "/dev/null")
	pod := simplePod("foo", map[string]string{})
	info := NewPodTLSKeyInfo(&pod, []corev1.Service{})
	secret, err := tls.SecretForKeyInfo(context.Background(), info)
	if err != nil {
		t.Error(err)
	}
	certData := secret.Data[certFilename]
	cert, err := x509.ParseCertificateRequest(certData)
	if err != nil {
		t.Error(err)
	}
	if len(cert.DNSNames) < 1 {
		t.Error("No DNS Names Defined")
	}

}
