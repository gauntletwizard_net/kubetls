package tlsService

//This is a service that creates certificates and hands them back to the pod as secrets

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/pem"
	goerrors "errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"strings"

	"gitlab.com/gauntletwizard_net/kubetls/util/wellknown"
	certsv1 "k8s.io/api/certificates/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	typedCerts "k8s.io/client-go/kubernetes/typed/certificates/v1"
	typedCore "k8s.io/client-go/kubernetes/typed/core/v1"
	csrUtil "k8s.io/client-go/util/certificate/csr"

	"software.sslmate.com/src/go-pkcs12"
)

var _ = fmt.Println

const (
	CSRPemType = "CERTIFICATE REQUEST"
	RSAPemType = "RSA PRIVATE KEY"

	DefaultKeyLength = 2048
)

var (
	// Copied from crypto/x509/x509.go
	oidExtensionExtendedKeyUsage = asn1.ObjectIdentifier{2, 5, 29, 37}

	extKeyUsages   = []byte{48, 20, 6, 8, 43, 6, 1, 5, 5, 7, 3, 1, 6, 8, 43, 6, 1, 5, 5, 7, 3, 2}
	standardUsages = []pkix.Extension{{
		Id:    oidExtensionExtendedKeyUsage,
		Value: extKeyUsages,
	}}

	// Kubernetes version of the above
	kubeStandardUsages = []certsv1.KeyUsage{certsv1.UsageDigitalSignature, certsv1.UsageKeyEncipherment, certsv1.UsageServerAuth, certsv1.UsageClientAuth}
)

// Service that creates a TLS cert and kicks it back to the pod for mounting
type TlsService interface {
	SecretForKeyInfo(ctx context.Context, info TLSKeyInfo) (*corev1.Secret, error)
}

type RealTlsService struct {
	k8s   kubernetes.Interface
	core  typedCore.CoreV1Interface
	certs typedCerts.CertificatesV1Interface

	// Labels is a list of labels that will be applied to each created secret, and required for making use of a secret.
	labels map[string]string

	// caCerts is the trust bundle to be passed to our clients. One or more x509 certs, usually including the one that our cert will be signed by.
	caCerts []*x509.Certificate
	// caPemBytes is the pem encoded form of these CA Certs.
	caPemBytes []byte
	// Truststore is the Java version of a CA PEM
	trustStore []byte

	// KeyLength sets the generated key siza
	KeyLength int
	// SignerName is passed to the kubernetes CSR Object.
	SignerName string
	// SpiffeDomain is the Spiffe Trust Domain SVIDs are generated for, if enabled:
	SPIFFEDomain string
}

func NewRealTlsService(cs kubernetes.Interface, KeyLength int, SignerName string, caFile string) TlsService {
	// Read in the Kubernetes CA File; we use both the parsed and raw forms
	caPemBytes, err := ioutil.ReadFile(caFile)
	if err != nil {
		panic(err)
	}
	der, _ := pem.Decode(caPemBytes)
	caCerts, err := x509.ParseCertificates(der.Bytes)
	if err != nil {
		fmt.Println("Error loading CACert:", err)
	}
	fmt.Printf("TLS Service Startup: Loaded %d CA Certificates\n", len(caCerts))

	// Set up our trust store
	trustStore, err := pkcs12.EncodeTrustStore(rand.Reader, caCerts, pkcs12.DefaultPassword)
	if err != nil {
		fmt.Println("Error formatting truststore", err)
	}

	return RealTlsService{
		k8s:        cs,
		core:       cs.CoreV1(),
		certs:      cs.CertificatesV1(),
		labels:     map[string]string{"owner": "kubetls"},
		caCerts:    caCerts,
		caPemBytes: caPemBytes,
		trustStore: trustStore,
		KeyLength:  KeyLength,
		SignerName: SignerName,
	}
}

// In case of colision, add a nonce that cannot be a valid service (not rfc1035 compliant).

func (t RealTlsService) SecretForKeyInfo(ctx context.Context, info TLSKeyInfo) (*corev1.Secret, error) {
	secretName := info.Name()
	fmt.Printf("Looking for secret %s/%s\n", info.Namespace(), secretName)
	existing, err := t.core.Secrets(info.Namespace()).Get(ctx, secretName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		fmt.Printf("Secret %s/%s not found, creating...\n", info.Namespace(), secretName)
		return t.CreateSecretForServices(ctx, info)
	} else if err == nil {
		fmt.Printf("Found existing secret %s/%s\n", info.Namespace(), secretName)
		return existing, err
	} else {
		fmt.Println("Get Failed: ", err)
		return nil, err
	}
}

const (
	keyFilename  = "tls.key"
	csrFilename  = "tls.csr"
	certFilename = "tls.crt"
)

func (t RealTlsService) CreateSecretForServices(ctx context.Context, info TLSKeyInfo) (*corev1.Secret, error) {
	secretToPass := &corev1.Secret{}
	secretToPass.Name = info.Name()
	secretToPass.Annotations = info.Annotations()
	secretToPass.Labels = t.labels
	secretToPass.Type = corev1.SecretTypeTLS

	keybytes, key, csr := t.csrForServices(info)

	cert, err := t.uploadAndApproveCSR(ctx, csr, info)
	if err != nil {
		fmt.Println("Failed to create CSR, ", err)
		return nil, err
	}

	pkcs12Store, pkcs12Password, err := t.convertToPkcs12(cert, &keybytes)
	if err != nil {
		fmt.Println("Failed to convert to Pkcs12 store, ", err)
	}

	secretToPass.Data = map[string][]byte{
		keyFilename:         key,
		csrFilename:         csr,
		certFilename:        cert,
		"pkcs12.certStore":  pkcs12Store,
		"pkcs12.password":   pkcs12Password,
		"ca.crt":            t.caPemBytes,
		"pkcs12.trustStore": t.trustStore,
	}

	return t.core.Secrets(info.Namespace()).Create(ctx, secretToPass, metav1.CreateOptions{})
}

func (t RealTlsService) convertToPkcs12(pemCert []byte, key *rsa.PrivateKey) ([]byte, []byte, error) {
	passphrasestring := pkcs12.DefaultPassword

	certBlock, _ := pem.Decode(pemCert)
	if certBlock == nil {
		return nil, nil, goerrors.New("Cannot extract cert in convertToPkcs12")
	}

	x509Cert, err := x509.ParseCertificate(certBlock.Bytes)
	if err != nil {
		return nil, nil, err
	}

	// Use the sslmate library to create a PKCS12 formated string
	pfxData, err := pkcs12.Encode(rand.Reader, key, x509Cert, t.caCerts, passphrasestring)
	if err != nil {
		return nil, nil, err
	}
	return pfxData, []byte(passphrasestring), nil
}

// uploadAndApproveCSR does what it says.
func (t RealTlsService) uploadAndApproveCSR(ctx context.Context, csr []byte, info TLSKeyInfo) (crt []byte, err error) {
	client := t.certs.CertificateSigningRequests()

	csrObject := certsv1.CertificateSigningRequest{}
	csrObject.Name = info.Name()
	csrObject.Annotations = info.Annotations()
	csrObject.Labels = t.labels
	csrObject.Spec.SignerName = t.SignerName
	csrObject.Spec.Request = csr
	csrObject.Spec.Usages = kubeStandardUsages

	createdCsrObject, err := client.Create(ctx, &csrObject, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	// This is the stamp we assign to the CSR object to let the cluster master sign it.
	approval := certsv1.CertificateSigningRequest{
		Status: certsv1.CertificateSigningRequestStatus{
			Conditions: []certsv1.CertificateSigningRequestCondition{{
				Type:           certsv1.CertificateApproved,
				Status:         corev1.ConditionTrue,
				Reason:         "Approved by TLS Service",
				Message:        "Foo Bar Baz",
				LastUpdateTime: metav1.Now(),
			}},
		},
	}
	approval.ObjectMeta = createdCsrObject.ObjectMeta

	approved, err := client.UpdateApproval(ctx, approval.Name, &approval, metav1.UpdateOptions{})
	if err != nil {
		return nil, err
	}

	certdata, err := csrUtil.WaitForCertificate(ctx, t.k8s, approved.Name, approved.UID)

	return certdata, nil
}

const KeyLength = 2048

/*
	csrForServices takes a set of Service Names, and generates a private key and CSR.

cn is the Common Name. In our case, it's the name of the pod or something.
*/
func (t RealTlsService) csrForServices(info TLSKeyInfo) (rawkey rsa.PrivateKey, keybytes, csr []byte) {
	key, err := rsa.GenerateKey(rand.Reader, t.KeyLength)

	if err != nil {
		panic(err)
	}

	subj := pkix.Name{
		CommonName: info.ServiceAccount(),
		// we can add more here, but it's not necessary
	}

	// https://github.com/spiffe/spire/blob/main/support/k8s/k8s-workload-registrar/README.md#service-account-based-workload-registration
	spiffeURI := url.URL{
		Scheme: wellknown.SPIFFEScheme,
		Host:   t.SPIFFEDomain,
		Path:   fmt.Sprintf("ns/%s/sa/%s", info.Namespace(), info.ServiceAccount()),
	}

	fmt.Printf("dns names: %s\n", strings.Join(info.DNSNames(), ","))
	template := x509.CertificateRequest{
		Subject:            subj,
		SignatureAlgorithm: x509.SHA256WithRSA,
		DNSNames:           info.DNSNames(),
		URIs:               []*url.URL{&spiffeURI},
		ExtraExtensions:    standardUsages,
	}

	csrBytes, _ := x509.CreateCertificateRequest(rand.Reader, &template, key)
	csrPem := &bytes.Buffer{}
	pem.Encode(csrPem, &pem.Block{Type: CSRPemType, Bytes: csrBytes})

	keyBytes, _ := x509.MarshalPKCS8PrivateKey(key)
	keyPem := &bytes.Buffer{}
	pem.Encode(keyPem, &pem.Block{Type: RSAPemType, Bytes: keyBytes})

	return *key, keyPem.Bytes(), csrPem.Bytes()
}

type MockTlsService struct {
}

func (m MockTlsService) CreateSecretForServices(pod corev1.Pod) []corev1.Secret {

	//Will eventually return the actual secret
	return nil
}
