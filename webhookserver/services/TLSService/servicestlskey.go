package tlsService

import (
	"fmt"
	"hash/fnv"
	"sort"
	"strings"

	corev1 "k8s.io/api/core/v1"
)

var _ TLSKeyInfo = ServicesTLSKey{}

// ServicesTLSKey is a structure that holds all of the metadata about a TLS key we are about to create
// It implements the business-logic around translating a service list into that metadata
type ServicesTLSKey struct {
	pod      corev1.Pod
	services []corev1.Service

	serviceNames []string
}

// NewKubeTLSKeyInfo does not check that the services are from the same namespace as the pod.
func NewKubeTLSKeyInfo(pod corev1.Pod, services []corev1.Service) (keyinfo ServicesTLSKey) {
	keyinfo.pod = pod
	keyinfo.services = services
	for _, s := range services {

		keyinfo.serviceNames = append(keyinfo.serviceNames, s.Name)
	}
	sort.Strings(keyinfo.serviceNames)
	return
}

func (k ServicesTLSKey) Annotations() map[string]string {
	return map[string]string{
		NamespaceKey: k.pod.GetNamespace(),
		ServicesKey:  strings.Join(k.serviceNames, ","),
	}
}

// Name returns a hashed name that will be used for both the CSR and secret
// Takes the sorted list of services, and returns a consistent hash for that list.
func (k ServicesTLSKey) Name() string {
	hash := fnv.New32()

	fmt.Fprint(hash, k.pod.GetNamespace())
	fmt.Fprint(hash, k.pod.Spec.ServiceAccountName)
	fmt.Fprint(hash, strings.Join(k.serviceNames, ","))

	return fmt.Sprintf("tls-%x", hash.Sum32())
}

// ServiceAccountName associated with the pod. This is used in the ASN1 CN attribute
func (k ServicesTLSKey) Account() string {
	return fmt.Sprintf("system:serviceaccount:%s:%s", k.Namespace(), k.pod.Spec.ServiceAccountName)
}

func (k ServicesTLSKey) ServiceAccount() string {
	return k.pod.Spec.ServiceAccountName
}

// Returns the list of DNS names associated with this key.
func (k ServicesTLSKey) DNSNames() []string {
	return k.serviceNames
}

func (k ServicesTLSKey) Namespace() string {
	return k.pod.GetNamespace()
}

func (k ServicesTLSKey) GenerateName() bool {
	// Service-based TLS keys don't have a matching hostname, so we don't override the name
	return false
}
