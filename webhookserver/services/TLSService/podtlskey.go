package tlsService

import (
	"fmt"
	"sort"
	"strings"

	corev1 "k8s.io/api/core/v1"
	utilrand "k8s.io/apimachinery/pkg/util/rand"
)

var _ TLSKeyInfo = PodTLSKey{}

type PodTLSKey struct {
	pod      *corev1.Pod
	services []corev1.Service

	serviceNames  []string
	generatedName bool
}

// NewKubeTLSKeyInfo does not check that the services are from the same namespace as the pod.
func NewPodTLSKeyInfo(pod *corev1.Pod, services []corev1.Service) (keyinfo TLSKeyInfo) {
	tlskey := PodTLSKey{
		pod:      pod,
		services: services,
	}

	fmt.Printf("Pod Name: %s, Generate: %s \n", pod.Name, pod.GenerateName)
	if pod.Name == "" {
		tlskey.generatedName = true
		pod.Name = GenerateName(pod.GenerateName)
		fmt.Println("Generated name", pod.Name)
	}

	for _, s := range services {
		tlskey.serviceNames = append(tlskey.serviceNames, s.Name)
	}

	// The pod's name itself is valid
	tlskey.serviceNames = append(tlskey.serviceNames, pod.Name)
	// A "Headless" service named <subdomain> can be used to access pods directly:
	if pod.Spec.Subdomain != "" {
		tlskey.serviceNames = append(tlskey.serviceNames, fmt.Sprintf("%s.%s", pod.Name, pod.Spec.Subdomain))
	}

	sort.Strings(tlskey.serviceNames)
	return tlskey
}

func (k PodTLSKey) Annotations() map[string]string {
	return map[string]string{
		NamespaceKey: k.pod.GetNamespace(),
		ServicesKey:  strings.Join(k.serviceNames, ","),
	}
}

// Name returns a name that will be used for both the CSR and secret
// For per-pod keys, this is simply the name of the pod
func (k PodTLSKey) Name() string {
	return k.pod.Name
}

// Account name of the pod. This is used in the ASN1 CN Field, to facilitate communication with kubernetes
func (k PodTLSKey) Account() string {
	return fmt.Sprintf("system:serviceaccount:%s:%s", k.Namespace(), k.pod.Spec.ServiceAccountName)
}

// ServiceAccountName associated with the pod. This is used in the ASN1 CN attribute
func (k PodTLSKey) ServiceAccount() string {
	return k.pod.Spec.ServiceAccountName
}

// Returns the list of DNS names associated with this key.
func (k PodTLSKey) DNSNames() []string {
	return k.serviceNames
}

func (k PodTLSKey) Namespace() string {
	return k.pod.GetNamespace()
}

func (k PodTLSKey) GenerateName() bool {
	return k.generatedName
}

// Copied from k8s.io/apiserver/pkg/storage/names/generate.go
const (
	// TODO: make this flexible for non-core resources with alternate naming rules.
	maxNameLength          = 63
	randomLength           = 5
	maxGeneratedNameLength = maxNameLength - randomLength
)

func GenerateName(base string) string {
	if len(base) > maxGeneratedNameLength {
		base = base[:maxGeneratedNameLength]
	}
	return fmt.Sprintf("%s%s", base, utilrand.String(randomLength))
}
