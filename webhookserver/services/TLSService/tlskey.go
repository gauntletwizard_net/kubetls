package tlsService

const (
	annotation_prefix = "kubetls.gauntletwizard.net/"

	// the Services annotation
	ServicesKey  = annotation_prefix + "services"
	NamespaceKey = annotation_prefix + "namespace"
)

// TLSKeyInfo represents one TLS Keypair's information.
type TLSKeyInfo interface {
	// Name returns a name that will be used for both the CSR and secret
	Name() string

	// Annotations returns a map of annotations to be applied to the CSR and secret
	Annotations() map[string]string

	// Account associated with the this key. This is used in the ASN1 CN attribute
	Account() string

	// ServiceAccount associated with the this pod.
	ServiceAccount() string

	// DNSNames associated with this key
	DNSNames() []string

	// Namespace that this key is created in
	Namespace() string

	// GenerateName is true if we intend to override the pod's name.
	GenerateName() bool
}
