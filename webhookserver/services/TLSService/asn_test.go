package tlsService

import (
	"bytes"
	"crypto/x509"
	"encoding/asn1"
	"encoding/pem"
	"testing"
)

var (
	oidExtKeyUsageServerAuth = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 3, 1}
	oidExtKeyUsageClientAuth = asn1.ObjectIdentifier{1, 3, 6, 1, 5, 5, 7, 3, 2}
)

const examplecsr = `
-----BEGIN CERTIFICATE REQUEST-----
MIIBkjCB/AIBADAOMQwwCgYDVQQDEwNmb28wgZ8wDQYJKoZIhvcNAQEBBQADgY0A
MIGJAoGBAKGTrHB+rxsBmKdQF5mjIsHdhAvE00D0dx+lOfzIBAYoGw8pz/EhRyfi
qHabkDfJBY5sndGfV8zLCnJ1YDJsXwsvWjnGMBKF0d7c+Ea1WF833+az/2b20uvw
nhz3qvKbuNpsCUzjxGzHC2b6LmBAP50NrgwBXNOg35JyeR6ZuL/xAgMBAAGgRTBD
BgkqhkiG9w0BCQ4xNjA0MBMGA1UdEQQMMAqCA2JhcoIDYmF6MB0GA1UdJQQWMBQG
CCsGAQUFBwMBBggrBgEFBQcDAjANBgkqhkiG9w0BAQsFAAOBgQAxOb9jgIngY/go
5RtKd6a5/jS3V1KmZAnc1HNEGGBJJnZKD9KvEi6Qb/C7VyYmmRCKa+dg3mxkC8Kv
DjDgf/puFfZurOWV/YhjywdSZg8ezS0yRKjx4g0zicyUM+9kU06rE2z+ImvkTb89
g3rNmo/AfzOV95VZ9Y2DTbH5vwjZDw==
-----END CERTIFICATE REQUEST-----
`

const examplecert = `
-----BEGIN CERTIFICATE-----
MIICQDCCAeagAwIBAgIIDhETy0ylG0IwCgYIKoZIzj0EAwIwazELMAkGA1UEBhMC
VVMxCzAJBgNVBAgTAldBMREwDwYDVQQHEwhJc3NhcXVhaDEaMBgGA1UEChMRTG9j
YWwgRGV2ZWxvcG1lbnQxDDAKBgNVBAsTA1lvdTESMBAGA1UEAxMJbG9jYWxob3N0
MB4XDTIwMTAzMTA1MDkzOFoXDTIxMDEzMDExMTQzOFowazELMAkGA1UEBhMCVVMx
CzAJBgNVBAgTAldBMREwDwYDVQQHEwhJc3NhcXVhaDEaMBgGA1UEChMRTG9jYWwg
RGV2ZWxvcG1lbnQxDDAKBgNVBAsTA1lvdTESMBAGA1UEAxMJbG9jYWxob3N0MFkw
EwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEvh89V0+BmxoPNmDmROJH7WQgX23v4am6
2CK69z+x0cLy5q4TRx1eu668hkYDey/AUiXNTKSc/xjWsMWMw8AT9qN0MHIwDgYD
VR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNV
HRMBAf8EAjAAMB0GA1UdDgQWBBQjZjPTd0Vj+HyK6ZIREuALJxo33zAUBgNVHREE
DTALgglsb2NhbGhvc3QwCgYIKoZIzj0EAwIDSAAwRQIgZ3+hkfmaWn29C97xR1+A
tL9CYqEMRuuKir9X0pdYMdoCIQCUdTa2zmn/5w3i2jOBJf9cqO9d7UbeX4Nfn7r7
QATnRQ==
-----END CERTIFICATE-----
`

// TestUsagesEncoding encodes our requested ExtendedKeyUsage value and verifies it against the embedded encoding
// The value doesn't change and including it as a bytes string saves us an ugly init and panic.
func TestUsagesEncoding(t *testing.T) {
	// KubeTLS certificates are used as both a client and server identifier for intra-cluster communication
	// See https://tools.ietf.org/html/rfc5280#section-4.2
	encoded, err := asn1.Marshal([]asn1.ObjectIdentifier{oidExtKeyUsageServerAuth, oidExtKeyUsageClientAuth})
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Encoded form of ASN.1 Extended Key Usages: %v", encoded)
	if !bytes.Equal(encoded, extKeyUsages) {
		t.Errorf("ASN.1 Encoded Key Usages is encoded incorrectly: Got: %v Wanted: %v", extKeyUsages, encoded)
	}
	csrBlock, rest := pem.Decode([]byte(examplecsr))
	t.Logf("Leftover bytes from CSR: %v", rest)
	csr, err := x509.ParseCertificateRequest(csrBlock.Bytes)
	if err != nil {
		t.Fatal(err)
	}
	for _, ext := range csr.Extensions {
		if ext.Id.Equal(oidExtensionExtendedKeyUsage) {
			if !bytes.Equal(ext.Value, encoded) {
				t.Errorf("ASN.1 Encoded Key Usages doesn't match example certificate: Got: %v Wanted: %v", ext.Value, encoded)
			}
		}
	}
}

func TestExampleCert(t *testing.T) {
	crtBlock, rest := pem.Decode([]byte(examplecert))
	t.Logf("Leftover bytes from Cert: %v", rest)
	cert, err := x509.ParseCertificate(crtBlock.Bytes)
	if err != nil {
		t.Fatal(err)
	}
	for _, ext := range cert.Extensions {
		if ext.Id.Equal(oidExtensionExtendedKeyUsage) {
			if !bytes.Equal(ext.Value, extKeyUsages) {
				t.Errorf("ASN.1 Encoded Key Usages doesn't match example certificate: Got: %v Wanted: %v", ext.Value, extKeyUsages)
			}
		}
	}

}
