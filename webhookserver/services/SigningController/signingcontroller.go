package SigningController

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"math/big"
	"time"

	pemconst "gitlab.com/gauntletwizard_net/kubetls/util/pem"
	"gitlab.com/gauntletwizard_net/kubetls/util/wellknown"
	certs "k8s.io/api/certificates/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

const (
	resyncPeriod = 30 * time.Second

	KeyUsage = x509.KeyUsageDigitalSignature + x509.KeyUsageKeyEncipherment

	// Labels used for Prometheus Metrics
	signerNameLabel = "signer_name"
	errorStateLabel = "error_state"
)

// Metrics
var (
	requestsMetric = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "signer_requests",
		Help: "Number of requests for this signer",
	}, []string{signerNameLabel})
	errorsMetric = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "signer_errors",
		Help: "Number of (non-fatal) errors encountered by this signer",
	}, []string{signerNameLabel, errorStateLabel})
	signedMetric = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "signer_signed",
		Help: "Number of requests completed by this signer",
	}, []string{signerNameLabel})
)

// A SigningController's job is to sign CSRs from the Kubernetes API
type SigningController struct {
	client  kubernetes.Interface
	factory informers.SharedInformerFactory

	// This signingController only signs requests for SignerName
	SignerName string

	signerCert *tls.Certificate
	duration   time.Duration
}

func (s *SigningController) OnAdd(obj interface{}) {
	s.OnUpdate(nil, obj)
}

func (s *SigningController) OnUpdate(oldObj interface{}, newObj interface{}) {
	csr, ok := newObj.(*certs.CertificateSigningRequest)
	if !ok {
		fmt.Println("Not a CSR: ", newObj)
		return
	}
	if csr.Spec.SignerName != s.SignerName {
		fmt.Println("Ignoring CSR, not for us", csr.Name)
		return
	}
	if len(csr.Status.Certificate) != 0 {
		fmt.Println("Certificate already generated, ignoring", csr.Name)
		return
	}
	fmt.Printf("Got csr named %s\n", csr.Name)
	requestsMetric.With(s.labels()).Inc()

	block, _ := pem.Decode(csr.Spec.Request)
	if block.Type != pemconst.CSRPemType {
		return
	}
	parsed, err := x509.ParseCertificateRequest(block.Bytes)
	if err != nil {
		fmt.Println("Invalid ASN1 Bytes on csr", csr.Name)
		errorsMetric.MustCurryWith(s.labels()).WithLabelValues("invalid_asn1").Inc()
		return
	}
	fmt.Printf("Got csr named %s for cn %s\n", csr.Name, parsed.Subject.CommonName)

	if len(csr.Status.Conditions) == 0 {
		fmt.Println("Certificate has no approvals yet, ignoring", csr.Name)
		// Should this be a metric so that there's a reason for every request?
		return
	}

	if s.ValidateApproval(csr) != nil {
		fmt.Println("Certificate approvals are invalid/lacking, ignoring")
		errorsMetric.MustCurryWith(s.labels()).WithLabelValues("invalid_approvers").Inc()
		return
	}

	csr.Status.Certificate = s.signCertificate(*parsed)
	fmt.Println(csr.Status.Certificate)
	_, err = s.client.CertificatesV1().CertificateSigningRequests().UpdateStatus(context.Background(), csr, meta.UpdateOptions{})
	if err != nil {
		errorsMetric.MustCurryWith(s.labels()).WithLabelValues("update_error").Inc()
		fmt.Printf("Certificate update failed for %s, error: %s\n", csr.Name, err)
		return
	}
	fmt.Printf("Certificate signed: %s\n", csr.Name)
	signedMetric.With(s.labels()).Inc()
}

func (s *SigningController) OnDelete(obj interface{}) {
	// Deletions are based on expiry (or rarely rejection); Ignore.
}

const (
	defaultDuration = time.Second * 864000 // 10 days
)

func FromClient(k8s kubernetes.Interface, cert tls.Certificate) (s *SigningController) {

	s = &SigningController{
		client:     k8s,
		factory:    informers.NewSharedInformerFactory(k8s, resyncPeriod),
		signerCert: &cert,
		SignerName: wellknown.KubetTLSSignerName,
		duration:   defaultDuration,
	}
	s.factory.WaitForCacheSync(nil)
	fmt.Println("synced")
	return
}

// Loop infinitely, running the controller
func (s *SigningController) Start() error {
	s.factory.Certificates().V1().CertificateSigningRequests().Informer().AddEventHandler(s)
	stop := make(chan struct{})
	defer close(stop)
	s.factory.Start(stop)

	for {
	}
	// shoud not happen
}

// ValidateObject returns nil if the certificate has been properly approved, otherwise it returns an appropriate error
func (s *SigningController) ValidateApproval(csr *certs.CertificateSigningRequest) error {
	return nil
}

// ValidateCertificate returns nil if the given CSR matches the format we expect,
// otherwise a (descriptive) error
func (s *SigningController) ValidateCertificate(csr x509.CertificateRequest) error {
	return nil
}

// SignCertificate signs a certificate with the signingcontroller's private key. This happens after validation.
// It returns the signed certificate as bytes in PEM form.
// TODO: Audit this. Fields returned should be precise.
func (s *SigningController) signCertificate(csr x509.CertificateRequest) (cert []byte) {

	// This controls the fields we recognize. We only copy these fields into the template.
	// https://golang.org/pkg/crypto/x509/#Certificate
	template := &x509.Certificate{
		SignatureAlgorithm: s.signerCert.Leaf.SignatureAlgorithm,

		PublicKeyAlgorithm: csr.PublicKeyAlgorithm,
		PublicKey:          csr.PublicKey,

		Version: 1,
		// TODO: Revisit
		SerialNumber: big.NewInt(1),
		Issuer:       s.signerCert.Leaf.Subject,
		Subject:      csr.Subject,
		// TODO: Not direclty time for testing.
		NotAfter:  time.Now().Add(s.duration),
		NotBefore: time.Now(),
		KeyUsage:  KeyUsage,

		// Rather than copy each SAN individually, use ExtraExtensions to copy them all at once.
		ExtraExtensions: csr.Extensions,
	}

	crt, err := x509.CreateCertificate(rand.Reader, template, s.signerCert.Leaf, csr.PublicKey, s.signerCert.PrivateKey)
	if err != nil {
		fmt.Println("Error signing", err)
		errorsMetric.MustCurryWith(s.labels()).WithLabelValues("signing_error").Inc()
		return nil
	}

	fmt.Println("ert", crt)
	block := pem.Block{
		Type:  pemconst.CertPemType,
		Bytes: crt,
	}

	return pem.EncodeToMemory(&block)
}

func (s *SigningController) labels() prometheus.Labels {
	return prometheus.Labels{signerNameLabel: s.SignerName}
}
