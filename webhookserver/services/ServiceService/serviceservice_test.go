package serviceService

import (
	"context"
	"sort"
	"testing"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/fake"
)

func TestInterfaceTypeConstraint(_ *testing.T) {
	var s ServiceService
	s = MockServiceService{}

	s.MatchingServices(context.TODO(), &metav1.ObjectMeta{})
}

type labelMap = map[string]string

func simpleService(name string, selector map[string]string) (s *corev1.Service) {
	s = &corev1.Service{}
	s.Name = name
	s.Spec.Selector = selector
	return
}

func simplePod(name string, labels map[string]string) (p *corev1.Pod) {
	p = &corev1.Pod{}
	p.Name = name
	p.Labels = labels
	return
}

func checkServicesEqual(services []corev1.Service, names []string) bool {
	if len(services) != len(names) {
		return false
	}
	serviceNames, _ := SortServices(services, nil)
	sort.Strings(names)
	for i := range names {
		if names[i] != serviceNames[i] {
			return false
		}
	}
	return true
}

func TestServiceMatch(t *testing.T) {
	services := []runtime.Object{
		simpleService("circles", map[string]string{"angles": "round"}),
		simpleService("rectangles", map[string]string{"angles": "right"}),
		simpleService("squares", map[string]string{"angles": "right", "sides": "even"}),
	}

	s := DumbServiceService{
		client: fake.NewSimpleClientset(services...),
	}
	matching, err := s.MatchingServices(context.Background(), simplePod("circle-1", labelMap{"pod-template-hash": "foobarbaz", "angles": "round"}))
	if err != nil {
		t.Error(err)
	}
	if !checkServicesEqual(matching, []string{"circles"}) {
		t.Errorf("Services not matching: matched: %v", matching)
	}

}

func TestEmptySelector(t *testing.T) {
	emptySelector := simpleService("kubernetes", map[string]string{})
	emptySelector.Spec.Selector = nil
	services := []runtime.Object{emptySelector}

	s := DumbServiceService{
		client: fake.NewSimpleClientset(services...),
	}
	matching, err := s.MatchingServices(context.Background(), simplePod("circle-1", labelMap{"pod-template-hash": "foobarbaz", "angles": "round"}))
	if err != nil {
		t.Error(err)
	}
	if !checkServicesEqual(matching, []string{}) {
		t.Errorf("Services not matching: matched: %v", matching)
	}
}
