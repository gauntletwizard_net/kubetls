package serviceService

import (
	"context"
	"sort"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/kubernetes"
)

// Service that watches Kuberenetes services and allows checking if pods match.
type ServiceService interface {
	MatchingServices(ctx context.Context, pod metav1.Object) ([]corev1.Service, error)
}

type MockServiceService struct {
}

func (m MockServiceService) MatchingServices(ctx context.Context, pod metav1.Object) ([]corev1.Service, error) {
	return []corev1.Service{{}}, nil
}

type DumbServiceService struct {
	client kubernetes.Interface
}

func NewDumbServiceService(client kubernetes.Interface) DumbServiceService {
	return DumbServiceService{
		client: client}
}

// SortServices is a utility function to take MatchingServices and turn into a canonical-form sorted list of service names
func SortServices(services []corev1.Service, err error) (cservices []string, e error) {
	for _, s := range services {
		cservices = append(cservices, s.Name)
	}
	sort.Strings(cservices)

	return cservices, err
}

func (s DumbServiceService) MatchingServices(ctx context.Context, pod metav1.Object) (matching []corev1.Service, err error) {
	client := s.client.CoreV1().Services(pod.GetNamespace())
	services, err := client.List(ctx, metav1.ListOptions{})
	if err != nil {
		return
	}

	for _, service := range services.Items {
		// An nil selector matches nothing, and expects "endpoints" to be owned elsewhere.
		// https://kubernetes.io/docs/concepts/services-networking/service/#services-without-selectors
		if service.Spec.Selector == nil {
			continue
		}
		selector := labels.Set(service.Spec.Selector).AsSelectorPreValidated()
		if selector.Matches(labels.Set(pod.GetLabels())) {
			matching = append(matching, service)
		}
	}
	return
}
