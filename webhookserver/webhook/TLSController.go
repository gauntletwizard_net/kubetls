package webhook

import (
	serviceService "gitlab.com/gauntletwizard_net/kubetls/webhookserver/services/ServiceService"
	tlsService "gitlab.com/gauntletwizard_net/kubetls/webhookserver/services/TLSService"

	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	api_admission "k8s.io/api/admission/v1beta1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	//	"k8s.io/apimachinery/pkg/watch"
	v1beta1 "k8s.io/api/admission/v1beta1"
	//	"k8s.io/api/core/v1"
	certificates_client "k8s.io/client-go/kubernetes/typed/certificates/v1beta1"
)

const (
	// SecretMountPath is a location that the KubeTLS server will automount secrets to.
	// It matches other automatically generated secrets, like the ServiceAccountToken and
	// AWS's OAUTH implementation.
	SecretMountPath = "/var/run/secrets/gauntletwizard.net/tls"
)

/*
TLSController is a controller that implements our Webhooks

	This assumes that the requests have come just from within the cluster,
	which should be enforced by checking the client certificate
*/
type TLSController struct {
	k8scerts certificates_client.CertificateSigningRequestInterface
	Ss       serviceService.ServiceService
	Tlss     tlsService.TlsService
}

// AcceptWebhook handles a Kubernetes AdmissionRequest
// https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#request
func (s TLSController) AcceptWebhook(w http.ResponseWriter, r *http.Request) {
	admissionRequest, err := parseAdmissionRequest(r)
	if err != nil {
		w.WriteHeader(500)
		fmt.Println("Malformed AdmissionReview request, ", err)
		fmt.Fprintln(w, "Malformed AdmissionReview request, ", err)
		return
	}

	/*
		This should check that we're actually looking at a pod, and accept anything else, to prevent misconfigurations. But it doesn't.
		var pod corev1.Pod
		switch object := admissionRequest.Request.Object.Object.(type) {
		case corev1.Pod:
			pod = object
		default:
			// Accept and return?
			return
		}
	*/
	pod := &corev1.Pod{}
	json.Unmarshal(admissionRequest.Request.Object.Raw, pod)
	// Set directly, because the object in the request doesn't include it for some reason
	pod.Namespace = admissionRequest.Request.Namespace
	services, err := s.Ss.MatchingServices(r.Context(), pod)
	if err != nil {
		w.WriteHeader(503)
		fmt.Println("Failed to retrieve servies: ", err)
		fmt.Fprintln(w, "Failed to retrieve servies: ", err)
		return
	}
	info := tlsService.NewPodTLSKeyInfo(pod, services)
	secret, err := s.Tlss.SecretForKeyInfo(r.Context(), info)
	if err != nil {
		w.WriteHeader(500)
		fmt.Println("Failed to create secret: ", err)
		fmt.Fprintln(w, "Failed to create secret: ", err)
		return
	}
	review := Mutate(*admissionRequest, pod, secret.Name, info.GenerateName())

	response, err := json.Marshal(review)
	if err != nil {
		w.WriteHeader(500)
		fmt.Println("JSON Failed to marshal, ", err)
		fmt.Fprintln(w, "JSON Failed to marshal, ", err)
		return
	}

	w.WriteHeader(200)
	w.Write(response)
}

// ServicesAcceptWebhook handles a Kubernetes AdmissionRequest
// It differs from the "standard" webhook in that it creates a secret for a set of (serviceaccount, []services),
// potentially speeding up admission time (but forefeiting the per-pod hostname)
func (s TLSController) ServicesAcceptWebhook(w http.ResponseWriter, r *http.Request) {
	admissionRequest, err := parseAdmissionRequest(r)
	if err != nil {
		w.WriteHeader(500)
		fmt.Println("Malformed AdmissionReview request, ", err)
		fmt.Fprintln(w, "Malformed AdmissionReview request, ", err)
		return
	}

	/*
		This should check that we're actually looking at a pod, and accept anything else, to prevent misconfigurations. But it doesn't.
		var pod corev1.Pod
		switch object := admissionRequest.Request.Object.Object.(type) {
		case corev1.Pod:
			pod = object
		default:
			// Accept and return?
			return
		}
	*/
	pod := &corev1.Pod{}
	json.Unmarshal(admissionRequest.Request.Object.Raw, pod)
	// Set directly, because the object in the request doesn't include it for some reason
	pod.Namespace = admissionRequest.Request.Namespace
	services, err := s.Ss.MatchingServices(r.Context(), pod)
	if err != nil {
		w.WriteHeader(503)
		fmt.Println("Failed to retrieve servies: ", err)
		fmt.Fprintln(w, "Failed to retrieve servies: ", err)
		return
	}
	info := tlsService.NewKubeTLSKeyInfo(*pod, services)
	secret, err := s.Tlss.SecretForKeyInfo(r.Context(), info)
	if err != nil {
		w.WriteHeader(500)
		fmt.Println("Failed to create secret: ", err)
		fmt.Fprintln(w, "Failed to create secret: ", err)
		return
	}
	review := Mutate(*admissionRequest, pod, secret.Name, info.GenerateName())

	response, err := json.Marshal(review)
	if err != nil {
		w.WriteHeader(500)
		fmt.Println("JSON Failed to marshal, ", err)
		fmt.Fprintln(w, "JSON Failed to marshal, ", err)
		return
	}

	w.WriteHeader(200)
	w.Write(response)
}

// DebugAcceptWebhook parses the request, prints it to stdout, and returns an "allow" review.
func (s TLSController) DebugAcceptWebhook(w http.ResponseWriter, r *http.Request) {
	// AdmissionReview is a k8s meta-object that contains a request and/or a response
	admissionRequest, err := parseAdmissionRequest(r)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprint("Malformed AdmissionReview request, ", err)))
		return
	}
	response := &api_admission.AdmissionReview{
		TypeMeta: metav1.TypeMeta{
			APIVersion: api_admission.GroupName,
			Kind:       "AdmissionReview",
		},
		Response: &api_admission.AdmissionResponse{
			UID:     admissionRequest.Request.UID,
			Allowed: true,
		},
	}
	if len(admissionRequest.Request.Name) != 0 {
		fmt.Println("Accepted admission request with name: ", admissionRequest.Request.Name)
	} else {
		fmt.Println("Accepted admission request without name")

	}
	w.WriteHeader(200)
	e := json.NewEncoder(w)
	e.Encode(response)
}

// DebugParseWebhook does a roundtrip Unmarshal -> Marshall of the received
func (s TLSController) DebugParseWebhook(w http.ResponseWriter, r *http.Request) {
	parsed, err := parseAdmissionRequest(r)
	// AdmissionReview is a k8s meta-object that contains a request and/or a response
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprint("Bad request, ", err)))
		return
	}
	e := json.NewEncoder(os.Stdout)
	e.Encode(parsed)
	w.WriteHeader(200)

}

// DebugCopyWebhook simply writes the HTTP Request body back to the response
func (s TLSController) DebugCopyWebhook(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	io.Copy(w, r.Body)
}

// parseAdmissionRequest parses an incoming AdmissionReview
func parseAdmissionRequest(r *http.Request) (*v1beta1.AdmissionReview, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, fmt.Errorf("Failed to read request body: %w", err)
	}
	admissionReview := &api_admission.AdmissionReview{}
	err = json.Unmarshal(body, admissionReview)
	if err != nil {
		err = fmt.Errorf("Failed to Unmarshal AdmissionReview: %w", err)
	}
	fmt.Printf("Recieved Admission Request from %s", r.RemoteAddr)
	return admissionReview, err
}

func Mutate(admReview v1beta1.AdmissionReview, pod *corev1.Pod, secretName string, generatedName bool) (mutated v1beta1.AdmissionReview) {
	type jsonPatch struct {
		Op    string      `json:"op"`
		Path  string      `json:"path"`
		Value interface{} `json:"value"`
	}
	var sPatch []jsonPatch
	if pod.Spec.AutomountServiceAccountToken == nil || *pod.Spec.AutomountServiceAccountToken {
		// Mount on every container
		for i := range pod.Spec.Containers {
			sPatch = append(sPatch, jsonPatch{
				Op:   "add",
				Path: fmt.Sprintf("/spec/containers/%d/volumeMounts/-", i),
				Value: map[string]interface{}{
					"name":      secretName,
					"mountPath": SecretMountPath,
				},
			})
		}
	}
	sPatch = append(sPatch, jsonPatch{
		Op:   "add",
		Path: "/spec/volumes/-",
		Value: map[string]interface{}{
			"name":   secretName,
			"secret": map[string]string{"secretName": secretName},
		},
	})

	// If we generated the name of the pod, we need to add a mutation that fixes it.
	if generatedName {
		sPatch = append(sPatch, jsonPatch{
			Op:    "add",
			Path:  "/metadata/name",
			Value: secretName,
		})
		sPatch = append(sPatch, jsonPatch{
			Op:   "remove",
			Path: "/metadata/generateName",
		})
	}

	patch, err := json.Marshal(sPatch)
	if err != nil {
		panic(err)
	}

	resp := v1beta1.AdmissionResponse{}
	resp.Allowed = true
	resp.UID = admReview.Request.UID
	pT := v1beta1.PatchTypeJSONPatch
	resp.PatchType = &pT

	resp.Patch = patch
	resp.Result = &metav1.Status{
		Status: "Success",
	}

	admReview.Response = &resp

	return admReview
}
