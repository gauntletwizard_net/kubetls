# Draft server and Client Certificates from your own CA
This tutorial shows you how to use [CFSSL](https://github.com/cloudflare/cfssl) along with the KubeTLS Libraries to manually test MTLS certificates and authentication. It is not suitable for production use. It does not cover the full concepts of TLS Certificate Generation, only serving as a practical demonstration of how to use CFSSL to generate keys.

# What we're going to do
First, we're going to generate a Certificate Authority. A Certificate Authority comprises at bare minimum a TLS Private Key and a self-signed Certificate - That's really all you need for development work. We'll then generate two keys - One for our server and one for our client - which will be used for our demonstration. Finally, we'll generate another self-signed certificate and demonstrate that our server will not accept it.

# Commands to run
    cd ca
    cfssl genkey -initca ca-csr.json  | cfssljson -bare

    # Create and then sign the server certificate
    cfssl genkey server-csr.json | cfssljson -bare
    cfssl sign -ca ../ca/cert.pem -ca-key ../ca/cert-key.pem cert.csr  | cfssljson -bare
    openssl verify -CAfile ../ca/cert.pem cert.pem

    cd ../client/
    cfssl genkey client-csr.json | cfssljson -bare
    cfssl sign -ca ../ca/cert.pem -ca-key ../ca/cert-key.pem cert.csr  | cfssljson -bare

# What we just did
First, we create a key for the Certificate Authority. The `-initca` flag to CFSSL causes it to also generate a self-signed certificate for this CA. This is the core of TLS; At the root of the TLS certificate tree is (one or more) trusted CA.  Then, we created a key and signing request for the server. We used the CA's key and certificate to turn that Signing Request into a Certificate, and then we verified that certificate against the CA's certificate, proving the chain of trust. We then repeated that process for a client certificate.
