# Overview
Work flows for Kubetls

## Version 18 and prior (to about version 6)

```mermaid
sequenceDiagram
    podcreation->>k8s: pod.spec
    activate k8s
    k8s ->> kubetls: admission
    activate kubetls
    Note over k8s, kubetls: kubetls admission hook
    kubetls ->> k8s: csr
    kubetls ->> k8s: authorize csr
    k8s ->> k8s: certificate
    Note over k8s: k8s signs csr creating certificate
    k8s ->> kubetls: certificate
    kubetls ->> k8s: secret
    Note over kubetls: kubetls creates secret with private key
    kubetls ->> k8s: modification of pod.spec
    Note over k8s, kubetls: kubetls update pod.spec adding secret
    deactivate kubetls
    k8s ->> podcreation: pod created
    deactivate k8s
```

## Version 19 and later (to about version 25 with trust zones)

```mermaid
sequenceDiagram
    podcreation->>k8s: pod.spec
    activate k8s
    k8s ->> kubetls: admission
    activate kubetls
    Note over k8s, kubetls: kubetls admission hook
    kubetls ->> k8s: csr
    kubetls ->> k8s: authorize csr
    k8s ->> signer: csr
    activate signer
    Note over signer: signer module signs csr creating certificate
    signer ->> k8s: certificate
    deactivate signer
    k8s ->> kubetls: certificate
    kubetls ->> k8s: secret
    Note over kubetls: kubetls creates secret with private key
    kubetls ->> k8s: modification of pod.spec
    Note over k8s, kubetls: kubetls update pod.spec adding secret
    deactivate kubetls
    k8s ->> podcreation: pod created
    deactivate k8s
```

# `kubetls` Walkthrough

## webhook setup

The pod admision webhook is setup in the
`k8s/webhookserver/mutatingwebhook.yaml` file. This setups the
validating, and mutating, admission webhook. The `rules` stanza filters
for only pod requests. The `clientConfig` stanza sets up the call to
the `kubetls` service in the `kubetls` namespace.
(The `webhookconfig.yaml` sets up and admission controller which does
not mutate the incomming request and is useful during debugging.)

The `Dockerfile` builds the `kubetls` image from the compiled golang
code. The `main` for `kubetls is in `cmd/webhookserver/main.go`. This
sets up the web server which handles in the incomming callbacks on pod
admission.

During deubgging the handler for `/debug/accept` is called. During production
operation the webhook calls the handler for `/webhook`. These handlers
exit for the `sslmutator` variable which is of the type
`TLSController`. The `TLSController` type is found in
`webhookserver/webhook/TLSController.go`. This file defines the
handler functions. 

## incomming requests

In `webhookserver/webhook/TLSController.go` the `AcceptWebhook`
function is called by the incomming admissions request (i.e. it
handles the web request from the k8s server which contains the pod
admission specification).

The pod specification is parsed. Then `kubetls` calls the
`ServiceService` to find all services in the pod's namespace. For each
service, if the service selector matches the pod, then we will return
that service. This is necessary because we want the service name in
the certificate request. Specifcially, clients of the pod will use the
service name as the hostname (or part of the hostname) when makeing
web requests to the service. We need this hostname for the certificate
request.

The pod could match many services (i.e. match the selector rules for
multiple services). In addition, the pod could be known my just it's
pod name. `kubetls` takes the complete list of service names and the
pod name and sorts this list to create a unique key. 

This unique key is lookedup becuase a certificate may have already
been generated for it, in which case `kubetls` returns it from the
cache.

## certificate generatation

If a new certificate needs to be generated then
`CreateSecretForServices` will be called. This will call
`csrForServices` which creates a secret key, public key and a new
certificate request (csr) object for the public key and list of service
names.

This new csr is then passed to `uploadAndApproveCSR`. This uses the
k8s client to ask k8s to create the csr on the cluster. Then it
creates a csr approval object which is uploaded to the cluster. The
approval object references the csr in the approval object metadata. 

Prior to version 1.19, the cluster would see the approval object and
sign the certificate. With version 1.19 and later, the cluster does
not sign user generated csr objects. 

Next `uploadAndApproveCSR` will use a k8s client function to wait for
the cluster to see the certificate reuqest and create the certificate.

If the certificate is not generated, then `kubetls` will wait here
forever, until the caller times out. 

## adding the secret to the pod

A new secret is created and it contains several object: the csr, the
certificate and the private key. It also contains different versions
of these object in different formats to make life easier for clients
in different languates (like Java).

This new secret is uploaded to the cluster.

Then the pod specification in modified to add the mounting of the
secret as a path in the pod's filesystem.

## information is retured

This mutated pod is marsheld into json and returned in the body of the
reply.

## cluster creates the pod

The cluster receives the reply, the pod is modified according to the
specifications in the reply and the pod is created.

## pod runs

The pod is started and the pod should be able to inspect the
filesystem to find the secret and use the certificate contained in the
secret to enable TLS communications. It can also use the certificate
for mutual tls authentication (mTLS).



# Notes

https://mermaid.js.org/syntax/sequenceDiagram.html

https://mermaid.live/

