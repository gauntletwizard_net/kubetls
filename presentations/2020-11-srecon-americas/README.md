
# Presentation Detail

Our presentation is formatted in plain markdown, with
[Marp](https://github.com/marp-team/marp-cli) being our tool of choice
for rendering output. See:

* https://marp.app/
* https://marpit.marp.app/
* https://github.com/marp-team/marp

# Operation

```
make
```

The `Makefile` contains the details to build the presentation
HTML. Then the HTML can be viewed in your favorite browser.

# High Level Flow

* Ishtio lead in
* How CAs work
  * Brief overview
  * Example of cfssl CA and/or OpenSSL CA
  * Compare to Let's Encrypt, MSFT CA, . . . 
* SSL Setup in Applications
  * Go Example
  * Java Example
  * Dotnet Core
* Mutating Controller
  * The plan
  * The details
* KubeTLS in Action
  * Deploy an infocontainer
  * Apply KubeTLS as the Mutating Controller
  * Deploy infocontainer again
  * Show differences
  * Deploy go app with only TLS
  * Deploy java app with only TLS
* Future work


# Sequence diagrams

This sections contains the plain text descriptions of our sequence
diagrams which are rendered with websequencediagrams.com

## TLS Secret Creation

```
title TLS Secret Creation

participant Kubernetes Master
participant Admissions Controller
participant Kubernetes Kubelet

Kubernetes Master-> +Admissions Controller: Pod Creation
note over Admissions Controller 
Create TLS Key
end note 
Admissions Controller -> +Kubernetes Master: Certificate Signing Request
Kubernetes Master-> -Admissions Controller: Signed Certificate
note over Admissions Controller 
Create TLS Secret
end note
Kubernetes Master-> -Admissions Controller:
note over Admissions Controller 
Mutate pod specificiationo to add mounted TLS Certificate
end note
Admissions Controller -> -Kubernetes Master: Mutated Pod Specification
Kubernetes Master-> -Kubernetes Kubelet: Run Pod
```
