---
title: KubeTLS 2023
theme: gaia
style: |
  section {
    color: #000;
    background-color: #ccc;
  }
  img[alt="Ted Hahn"] { height: 100px; }
  img[alt="Mark Hahn"] { height: 100px; }
  img[alt="TCB Logo"] { height: 66px; }
  img[alt="Qualys Logo"] { height: 66px; }
  img[alt="Session Feedback"] { height: 150px; }
  img[alt="Certificate Authorities"] { height: 470px; }
  img[alt="Limiting Trust Scope"] { height: 425px; }
  img { height: 490px; }
---
# Improving Secure Pod-to-Pod Communication Using Trust Bundles

<!-- https://sched.co/1FV2R - 35 minutes (leave time for questions) -->
<!-- *Improving Secure Pod-to-Pod Communication Within Kubernetes Using Trust Bundles* -->

![Ted Hahn](https://www.tcbtech.com/wp-content/uploads/2020/11/ted.jpg) Ted Hahn, ![TCB Logo](images/TCB_Logo_Full.png)
thahn@tcbtech.com

<!-- Ted Hahn is an SRE for hire working on planet-scale distributed systems. His clients include Epic Games and startups in Seattle and New York. -->

![Mark Hahn](https://www.tcbtech.com/wp-content/uploads/2020/11/mark.jpg) Mark Hahn, ![Qualys Logo](images/Qualys_Logo_Full.png)
mhahn@qualys.com

<!-- Mark Hahn is a Security Solutions Architect for Cloud and Containers at Qualys. He works on securing cloud native environments for the most demanding customers worldwide. -->

## [tcbtech.com/cncf-kubetls](https://tcbtech.com/cncf-kubetls)

<!-- Project repo: https://gitlab.com/gauntletwizard_net/kubetls -->

<!-- We are father son team that put this idea and demonstration together. -->

<!-- mark then ted -->
---
# Achieving Mutual TLS - Secure Pod-to-Pod communication 

"Every Kubernetes pod should include a SSL Certificate, verifying its identity."

This should be signed automatically, and be specific to each pod.

<!-- In 2020 we built KubeTLS, a admissions-webhook pod, which mutates
the pod specific to include a certificate which is created automaticaly. -->

We have updated KubeTLS to modularize the certificate creation and improve the flow.

## [tcbtech.com/cncf-kubetls](https://tcbtech.com/cncf-kubetls)

<!-- Mark -->

<!-- 
Also KubeTLS v1.0 was broken by changes in kubernetes 
- Version 19
  - breaking changes in the certificate object
  - the kubernetes master no longer signs non-system csrs
- Version >25 (tbd)
  - trust bundles implement a domain of trust
  - KubeTLS allows you to limit scope to a smaller domain of trust
-->

--- 
# What is a TrustBundle?

Note: This is a forward looking feature, [KEP-3257](https://github.com/kubernetes/enhancements/pull/3258), that has yet to be implemented. We want to show some of the possible usecases.

A Trust Anchor, or "Root of trust", is a cryptographic entity that you trust implicity. Typically you express this as an X.509 certificate with the CA bit set. A Trust Bundle one or more trust anchors combined together, with the same implicit trust. 

You already use a Trust Bundle - The Web Roots provided by your Base OS or by your Web Browser. 

<!-- Trusting something implicitly is fine, if it's not very broad. "The whole web" is very broad, so browsers implement a wide variety of additional checks to keep tabs on the CAs in the Web Roots trust bundle. For programatic communication between systems, we should view that level of verification as aspirational but unnecessary, because each trust bundle should be of a much smaller scope - A partnership or an organization. -->

<!-- Ted -->

---
# Brief detour - Certificate Authorities

![Certificate Authorities](images/Certificate_Authorities.png)

<!-- Certificate Authorities
- There is a top level CA with a root certiicate
- This root certificate is "self signed" (infamously)
- This root CA and key is usually stored offline
- The root CA signs several intermediate CA certificates
- These intermediate CAs are the workhorse of the CA
- Leaf/workload certificates are signed by the intermediate CA
- A typical workload cert includes the intermediate certs in PEM format
- This makes validating a cert easy; no need to find the intermediate certs
- A validator will have the root cert in its trust bundle
-->

<!-- Mark -->

--- 
# How to use TrustBundles?

- TrustBundles implment small scale scope of trust
- "The Web Trust Bundle" is one of the topmost layers of most docker images
- Docker Images should be small  <!-- For the common case of linux on kubernetes -->
- ClusterTrustBundles can be mounted like ConfigMaps, replacing "The Web Trust Bundle"
- Rapid Updates to TrustBundles

<!-- Once TrustBundles are fully implemented, then the "Web Trust Bundle" can be removed from the build phase of images -->

<!-- The Google "Distroless" container image contains just [four things](https://github.com/GoogleContainerTools/distroless/blob/main/base/README.md#documentation-for-gcriodistrolessbase-and-gcriodistrolessstatic) - Of which the CA Bundles are by far the largest. -->

<!--
Out of Scope: Windows, MacOS, BSD, Android, etc. because it is on Kubernetes
Caveats: Java which uses it's own root of trust format for legacy reasons
-->

<!-- Ted -->

--- 
# Limiting Trust Scope

![Limiting Trust Scope](images/Limited_Trust_Scope.png)

<!-- Mark -->

---

# Limiting Trust from KeyNote

![Limiting Trust from KeyNote](images/Keynote.png) 

<!-- refering back to the KeyNote on 
The Future of CloudNative Security
Zack Butcher put up this slide
We agree, and our KubeTLS provides the first three
-->

<!-- Mark -->


---
# What is KubeTLS

KubeTLS is about automatically injecting certificates that
provide workload identity into every pod and every containter
in a cluster. 

These Certificates provide Privacy, Authentication and
Authorization.

These Certificates work with TrustBundles to assist in mutual system identification.

<!-- We will unpack that sentence because there is a lot in
each part of it. -->

<!-- Ted -->

---
# Secure Networking on Kubernetes

- The complicated way: Sidecars or CNI with network policies
  - Sidecars add latency, and a management layer separate from the applications
  - Network policies add that management layer separate from the applications

- The native way: All application containers natively support TLS
  - Applications are in charge of security
  - Applications implement business specific security logic

<!-- istio , linkerd -->
<!-- calico, flannel, weave -->

<!-- Mark -->

---
# KubeTLS assists with implementing TLS Everywhere

<!-- Your Cisco Router or AWS Network Backplane sniffing packets (network tap) are an attack vector. -->
- Using TLS natively everywhere eliminates various attack vectors by encrypting all traffic inside the applcation:
  - Privacy is provided by TLS
  - Authentication is provided by mTLS using the TrustBundle.
  - Authorization is provided by inspection of the client provided certificate.

<!-- Ted -->

---
# Unpacking what is KubeTLS 

- KubeTLS is an [admission controller](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/) using `MutatingWebhookConfiguration` object.
- KubeTLS provides three files by modifying the pod on admission - 
  - A Private Key
  - A Matching Certificate, signed by our trust bundle
  - The trust bundle itself (since KEP-3257 is TBD)
- These are the building blocks of a TLS native architecture.
<!-- We have opinions about how to build that TLS native architecture. -->

<!-- Guess what, its not SPIFFE -->

<!-- Mark -->

---
# Our opinions

We have strong opinions on TLS
- Your applications should speak only TLS
- Your internal applications should not use the web root of trust
- Your need to design your organization's zones of trust
- Your internal applications should use an internal CA
- Your applications should present a client certificate
- You should validate the client certificate as caller identity

<!-- start simply adminstering your CA -->

<!-- Ted -->

---
# What not to do

![Not All Traffic is Encyrpted](images/Not_Encrypted.png) 

<!-- 
- side cars do not encrypt all traffic 
- side cars use complicated virtual networking tricks
- moreover, the people how configure the sidecars are not the application developers 
- so there is a disconnect between cluster admins and application developers 
- the application developers understand the problem domain and architecture
better than a separate cluster/sidecar administration team.
- This also applies to complex CNI implementations

-->

<!-- Mark -->

---
# Our Opinion

![Our Opinion on Sidecars](images/Our_Opinion.png) 

<!-- 

Be very quick on this slide, details follow on break out slides

This *looks* complicated but it is not. It's built of a simple building block that is repeated and modified in regular ways 

-->

<!-- There is a lot here to unpack. Lets take it step by step in the next view slides -->

<!-- Mark -->

---
# Internal API Server

![Internal API Server](images/Internal_API_Server.png)

<!-- Here an Internal API server been provided: 
- A certificate specific to the service it implements
- A private key for that certificate so it can decrypt incomming traffice
- The "trust bundle" representing our internal (and private) CA
- The service certificate can also be used for client requests downstream
- Because the TLS certficate is internal, other callers will need to trust the internal CA trust bundle
  - Or ignore it
- The internal API server should require and validate a client provided certifcate
- It should use that client provided certifcate as the client's identity for authorization
- It should use it's certificate to make downstream calls
- This positively identifies this server/caller as this particular service for authorization 

-->

<!-- Ted -->

---
# KubeTLS Certificate Distribution

![KubeTLS Cert Distribution](images/KubeTLS_Cert_Distribution.png)

<!--
- KubeTLS works with your CA infrastructure to create workload certs in realtime
- It is an admission controller
- It creates certificates for continaters/pods
- Mounts them as secrets
-->

<!-- Ted -->

---

# Our Opinion on Partner Trust

![Partner Trust Scenarios](images/Partner_Trust.png)

<!-- 
- Top is better: Renewals are easily automated
- Trust Bundles are much longer lived (CA certifcates)

-->

<!-- Mark -->

---
# KubeTLS Certificate Creation Details

- When a pod is created KubeTLS's admission controller is called
- KubeTLS looks up container's service (via metadata)
- KubeTLS will create and approve a certificate signing request (csr)
- The certificate manager will do magic to create a cert
- KubeTLS creates a secret with:
  - private key, the service cert, the root CA cert
- KubeTLS attaches the secret to all containers in the pod
- KubeTLS responds to the pod adminssion web call

<!--
- KubeTLS works with your CA infrastructure to create workload certs in realtime
- When a pod is created KubeTLS's admission controller web hook is called
- KubeTLS looks up container's service (via metadata)
- If KubeTLS already has the cert in a cache skip ahead
- KubeTLs will create a private key for a csr/cert-to-be
- KubeTLS will create a certificate signing request (csr)
  - as a k8s object
- KubeTLS will approve the csr
  - by ammending the csr k8s object
- A certificate manager will be notified of an approve csr
- The certificate manager will do magic to create a cert
  - as a k8s object
- KubeTLS will notice the cert is now created
- KubeTLS squirrels away the cert in its cache
- KubeTLS pulls the cert from the cache
- KubeTLS creates a k8s secret object with:
  - private key
  - the service cert (aka the workload cert)
  - The service cert PEM file should have intermediate certs
  - the root CA cert
- KubeTLS attaches the cert to the pod
  - It ammends the pod admission request
  - Mounts the secret as a volumne in a well-known location
  - The mount is done on all containers in the pod
- KubeTLS replys to the pod adminssion web call
  - With an approval
  - and the pod ammendments
-->
 
<!-- Ted -->

---
# KubeTLS X.509 Key Fields 

- Common Name 
  - Name of the pod
- Subject Alternative Name (SAN)
  - DNS with the name of the services
  - URI with the SPIFFE SVID
- Key usage and extended key usage
  - Identified as Web server and client
- Issuer (aka signer) identification

<!-- Mark -->

<!-- for SAN types see: https://www.cryptosys.net/pki/manpki/pki_x509extensions.html , https://www.rfc-editor.org/rfc/rfc5280 pg 38, https://www.rfc-editor.org/rfc/rfc6125 -->

---
# Demo Time

Demo time, open the command line
And let you out into the shell
Demo time, turn all of the servers on
Over every pod and every service

Demo time, one last call for changes
So, finish your commit or pr
Demo time, you don't have to log out
But you can't stay here
. . . 
          (apologies to Semisonic)

<!--
Demo time, open all the shells
And let you out into the command line
Demo time, turn all of the servers on
. . . 
          (apologies to Semisonic)
-->

<!--
kubectl config use-context demo
List *mutating* admission contoller 
        kubectl get mutatingwebhookconfigurations.admissionregistration.k8s.io

Show the pod spec (can the deployment yaml file)
        vim examples/golang-grpc-enhanced/greeter_server.yaml

List all secrets and CRs (of which there one test)
        kubectl get secrets 

depoly the pod (apply -f deployment)
        kubectl apply -f examples/golang-grpc-enhanced/greeter_server.yaml

List all secrets and CRs (of which there test a test)
        kubectl get secrets

describe the pod (with the addition of the secret mount)
        kubectl describe pod greeter-server-<tab>

Get this contents of the secret
        kubectl get secret greeter-server-<tab> -o yaml

Decode the X.509 Cert
        kubectl get secret greeter-server-<tab> -o json | jq -r '.data."tls.crt" | @base64d' | openssl x509 -noout -text

Point out the SAN with the SVID

Call greeter with valid credentails

Call greeter with incorrect credentails

-->

---
# Wrap up

- Having certificates automatically populated is easy

- Developers should know/learn how to use mTLS

- KubeTLS is "identity policy" agnostic

- Once eveybody is establishing authentication through mTLS we can move to establishing authorization through mTLS

<!-- Mark -->

---
# Future Directions

- Private keys should be generated on nodes

- KubeTLS will move to a CSI model 

- This should be built into Kubernetes

<!-- Ted -->

---

### Repository
#### https://gitlab.com/gauntletwizard_net/kubetls

### Presentation Slides
#### [tcbtech.com/cncf-kubetls](https://tcbtech.com/cncf-kubetls)

#### Session Feedback link:
![Session Feedback](images/session-qr.png)

    
---
# Additional Notes

The following slides and notes are useful.

- Auto mount service account tokens
  - https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/

---
# KubeTLS Admission Webhook

<style scoped>
pre {
  font-size: 65%;
}
</style>

```yaml
apiVersion: admissionregistration.k8s.io/v1
kind: MutatingWebhookConfiguration
webhooks:
- admissionReviewVersions:
  - v1beta1
  clientConfig:
    caBundle: <<X.509 Cert (base64 encoded)>>
    service:
      name: kubetls
      namespace: kubetls
  name: kubetls.gauntletwizard.net
  rules:
  - apiGroups:
    - ""
    apiVersions:
    - v1
    operations:
    - CREATE
    resources:
    - pods
...
```

<!-- Ted -->

