package main

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	tlsService "gitlab.com/gauntletwizard_net/kubetls/webhookserver/services/TLSService"
	"gitlab.com/gauntletwizard_net/kubetls/webhookserver/util/k8s"
)

func main() {
	fmt.Println("vim-go")

	client, _ := k8s.NewK8sClient()
	tls := tlsService.NewRealTlsService(client, tlsService.DefaultKeyLength, "test.gauntletwizard.net/kubetls", "/dev/null")
	//tls := tlsService.NewRealTlsService()
	pod := &corev1.Pod{}
	pod.Name = "foo"
	services := []corev1.Service{{ObjectMeta: metav1.ObjectMeta{Name: "foo"}}, {ObjectMeta: metav1.ObjectMeta{Name: "bar"}}}
	info := tlsService.NewPodTLSKeyInfo(pod, services)
	tls.SecretForKeyInfo(context.TODO(), info)
}
