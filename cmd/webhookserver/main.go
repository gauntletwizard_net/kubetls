package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	serviceService "gitlab.com/gauntletwizard_net/kubetls/webhookserver/services/ServiceService"
	tlsService "gitlab.com/gauntletwizard_net/kubetls/webhookserver/services/TLSService"
	"gitlab.com/gauntletwizard_net/kubetls/webhookserver/util/k8s"
	"gitlab.com/gauntletwizard_net/kubetls/webhookserver/webhook"
)

var (
	listenAddr = flag.String("listenAddr", ":8443", "Address to listen on")
	certFile   = flag.String("certFile", "/var/tls/tls.crt", "TLS Certificate")
	keyFile    = flag.String("keyFile", "/var/tls/tls.key", "TLS Key")

	keyLength   = flag.Int("keybytes", tlsService.DefaultKeyLength, "RSA Key Size")
	SignerName  = flag.String("signername", "gauntletwizard.net/kubetls", "Name of Signer on CSRs")
	trustbundle = flag.String("trustbundle", "/var/tls/ca.crt", "File containing the trust bundle that should be injected into secrets")
)

func main() {
	flag.Parse()
	client, _ := k8s.NewK8sClient()
	tlss := tlsService.NewRealTlsService(client, *keyLength, *SignerName, *trustbundle)
	ss := serviceService.NewDumbServiceService(client)
	sslmutator := webhook.TLSController{
		Tlss: tlss,
		Ss:   ss,
	}
	http.HandleFunc("/debug/accept", sslmutator.DebugAcceptWebhook)
	http.HandleFunc("/debug/copy", sslmutator.DebugCopyWebhook)
	http.HandleFunc("/debug/parse", sslmutator.DebugParseWebhook)
	http.HandleFunc("/webhook", sslmutator.AcceptWebhook)
	http.HandleFunc("/webhook/services", sslmutator.ServicesAcceptWebhook)
	http.Handle("/metrics", promhttp.Handler())
	log.Printf("Listening on %s", *listenAddr)
	log.Fatal(http.ListenAndServeTLS(*listenAddr, *certFile, *keyFile, nil))
}
