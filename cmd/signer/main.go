package main

// This package implements a signer, indepedent of the webhook server. It creates the same object and registers it more-or-less the same way. It is intended mostly for testing.

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"

	"gitlab.com/gauntletwizard_net/kubetls/util/wellknown"
	"gitlab.com/gauntletwizard_net/kubetls/webhookserver/services/SigningController"
	"gitlab.com/gauntletwizard_net/kubetls/webhookserver/util/k8s"
)

var (
	signingKey  = flag.String("signingKey", "/var/tls/tls.key", "Path to the signer's private key")
	signingCert = flag.String("signingCert", "/var/tls/tls.crt", "Path to the signer's CA (Or intermediate) Certificate")

	signerName = flag.String("signerName", wellknown.KubetTLSSignerName, "Path to the signer's CA (Or intermediate) Certificate")
)

func main() {
	flag.Parse()

	client, _ := k8s.NewK8sClient()

	cert, err := tls.LoadX509KeyPair(*signingCert, *signingKey)
	if err != nil {
		fmt.Print(fmt.Errorf("Error loading Signing Certificate: %w", err))
		return
	}
	// tls.LoadX509KeyPair helpfully parses the x509 certificate for correctness, and then discards it. Oh well.
	cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		// This should not be possible because it was already parsed and validated
		fmt.Print(fmt.Errorf("Error Parsing Signing Certificate: %w", err))
	}

	s := SigningController.FromClient(client, cert)
	s.SignerName = *signerName
	s.Start()
}
