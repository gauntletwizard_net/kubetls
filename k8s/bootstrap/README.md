# Overview 

This folder contains bootstrap tooling for Kubetls. When first
admitting the admission controller, it needs to have it's own secret
volume created with an appropriately signed key.
https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#service-reference

## Bootstrapping using a preconfigured signer
This part of the tutorial assumes you've already configured a signer. 
Create key:
```
    cfssl genkey cfssl-req.json |cfssljson -bare
```

Get key signed by K8s:
```
# create csr, approve, get crt
    echo {} | jq --rawfile csr cert.csr '{apiVersion: "certificates.k8s.io/v1", kind: "CertificateSigningRequest", metadata: {name: "kubetls-bootstrap"}, spec: {signerName: "gauntletwizard.net/kubetls", request: $csr| @base64, usages: ["server auth", "client auth", "digital signature", "key encipherment"]}}' | kubectl create -f -
    kubectl certificate approve kubetls-bootstrap
    kubectl get csr kubetls-bootstrap -o json | jq -r  '.status.certificate | @base64d' > cert.crt
    openssl x509 -in cert.crt -text -noout
```

Create secret:
```
kubectl create secret tls kubetls-bootstrap --key cert-key.pem --cert cert.crt
```

## Development instructions
### Create a selfsigned cert and install into Kubernetes
Create key:
```
    cfssl selfsign cfssl-req.json |cfssljson -bare bootstrap
```

Create secret:
```
    kubectl -n kubetls create secret tls kubetls-bootstrap --key bootstrap-key.pem --cert bootstrap.pem
```

Apply config:
```
    kubectl create --dry-run -f ../webhookserver/mutatingwebhook.yaml -o json | jq  '.webhooks[].clientConfig.caBundle = ($cert | @base64)' --rawfile cert bootstrap.pem | kubectl apply -f -
```


### Create and sign yourself a local self-signed certificate.
    sudo mkdir /var/tls
    cd /var/tls
    cfssl selfsign localhost ~/code/kubetls/k8s/bootstrap/cfssl-dev.json | sudo ~/go/bin/cfssljson -bare
    sudo chmod a+r *
    sudo mv cert-key.pem tls.key
    sudo mv cert.pem tls.crt
