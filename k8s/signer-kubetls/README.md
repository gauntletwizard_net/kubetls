# KubeTLS Signer
This is a configuration for KubeTLS's simple signer, which has just basic logic - It signs CSRs.

# Setup
This initializes a CA and stores it in a secret in your cluster, for use by the basic signer. Again, not production ready, just a reasonable demo. 

Generate the CA key:
    cfssl gencert -initca certs/ca.json | cfssljson -bare certs/ca
    kubectl -n kubetls create secret tls kubetls-signer-ca --key certs/ca-key.pem --cert certs/ca.pem

Start KubeTLS-signer
    kubectl apply -f ./

Generate the KubeTLS serving key and cert
    cfssl genkey ../bootstrap/cfssl-req.json | cfssljson -bare certs/serving
    echo {} | jq --rawfile csr certs/serving.csr '{apiVersion: "certificates.k8s.io/v1", kind: "CertificateSigningRequest", metadata: {name: "kubetls-bootstrap"}, spec: {signerName: "gauntletwizard.net/kubetls", request: $csr| @base64, usages: ["server auth", "client auth", "digital signature", "key encipherment"]}}' | kubectl create -f -
    kubectl get csr kubetls-bootstrap -o json | jq -r  '.status.certificate | @base64d' > certs/serving.pem

Create the secret for KubeTLS webhook
    kubectl -n kubetls create secret tls kubetls-bootstrap --key certs/serving-key.pem --cert certs/serving.pem
